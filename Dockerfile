# build environment
FROM node:8-jessie as builder
# load build posfix
ARG BUILD_ENV

# ssh git
ARG SSH_PRIVATE_KEY
ARG SSH_SERVER_HOSTKEYS
RUN mkdir /root/.ssh/
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa
RUN echo "${SSH_SERVER_HOSTKEYS}" > /root/.ssh/known_hosts
RUN chmod 0600 /root/.ssh/id_rsa

# set working directory
RUN mkdir /app
COPY . /app/.
WORKDIR /app
# install and cache app dependencies
RUN yarn
# build
RUN yarn build"${BUILD_ENV}"


# production environment
FROM nginx:1.13.9-alpine
COPY --from=builder /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]








