const getCurrentProjectHostname = (hostname = '') => {

	const chsHostnames = ['staging.chs.harvard.edu', 'staging.chs.harvard.local', 'chs.harvard.edu', 'chs.harvard.local', 'localhost'];

	if (
		window
		&& window.location.hostname
		&& !~chsHostnames.indexOf(window.location.hostname)
	) {
		hostname = window.location.hostname;
	}

	if (window.location.hostname === 'localhost' && window.location.port === '9009') {
		hostname = 'ahcip.chs.harvard.edu';
	}


	if (hostname.indexOf('ahcip') >= 0) {
		hostname = hostname.replace('ahcip', 'homer'); 
	}

	// regularlize development domain
	if (hostname && hostname.endsWith('chs.harvard.local')) {
		hostname = hostname.replace('chs.harvard.local', 'chs.harvard.edu');
	}

	// beta subdomain for testing production tenants
	hostname = hostname.replace('.beta', '')
	hostname = hostname.replace('.staging', '')

	// remove latter half of domain
	hostname = hostname.replace('.chs.harvard.edu', '')
	hostname = hostname.replace('.chs.orphe.us', '')

	return hostname;
};

export default getCurrentProjectHostname;
