import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import TimeAgo from 'react-timeago';
import ReactPlayer from 'react-player';
import IconDescription from '@material-ui/icons/Description';

const images = [
	'http://iiif.orphe.us/o_and_p.jpg/square/350,/0/default.jpg',
	'http://iiif.orphe.us/achilles_2.jpg/square/350,/0/default.jpg',
	'http://iiif.orphe.us/odysseus.jpg/square/350,/0/default.jpg',
	'http://iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
	'http://iiif.orphe.us/school-athens2.jpg/square/350,/0/default.jpg',
	'http://iiif.orphe.us/hector.jpg/square/350,/0/default.jpg',
	'http://iiif.orphe.us/ajax_achilles_3.jpg/square/350,/0/default.jpg',
	'http://iiif.orphe.us/iris.jpg/square/350,/0/default.jpg'
]


const Card = (props) => {

	const {
		to,
		href,
		title,
		textShort,
		textLong,
		assetImageSrc,
		assetImageWidth,
		assetImageHeight,
		assetImageAlt,
		assetVideoSrc,
		assetAudioSrc,
		assetPdfSrc,
		assetMediaUrl,
		// asset3DSrc,
		metadata,
		footer,
		horizontal,
		loading,
		classes,
		type,
		counts
	} = props;
	const _classes = classes || [];

	_classes.push('card');

	if (horizontal) {
		_classes.push('-horizontal');
	}

	if (loading) {
		_classes.push('-loading');
	}

	if (true || assetImageSrc) {
		_classes.push('-media');
	}
	if (type === 'text') {
		// assetImageSrc='http://iiif.orphe.us/o_and_p.jpg/full/full/0/default.jpg'
	}
	function getRandomInt(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	const cardContent = (
		<React.Fragment>
			{title &&
				<div className="cardTitle">{title}</div>
			}
			{textShort &&
				<div className="cardTextShort">{textShort}</div>
			}
			{textLong &&
				<div className="cardTextLong">{textLong}</div>
			}
			{assetImageSrc &&
				<div className="cardMedia">
					<img src={assetImageSrc} alt={assetImageAlt} width={assetImageWidth} height={assetImageHeight} />
				</div>
			}
			{(type && type === 'text') &&
				<div className="cardMedia">
					<img src={images[getRandomInt(0, images.length-1)]} />
				</div>
			}

			{ (type && type === 'commentary' && !assetImageSrc) &&
				<div className="cardMedia">
					<img src={images[getRandomInt(0, images.length-1)]} />
				</div>
			}

			{assetVideoSrc &&
				<div className="cardMedia cardMediaAV cardMediaVideo">
					<ReactPlayer
						url={assetVideoSrc}
						controls
						width="560px"
						height="320px"
					/>
				</div>
			}
			{assetAudioSrc &&
				<div className="cardMedia cardMediaAV cardMediaAudio">
					<ReactPlayer
						url={assetAudioSrc}
						controls
						width="560px"
						height="54px"
					/>
				</div>
			}
			{assetPdfSrc &&
				<div className="cardMedia cardMediaAV cardMediaPDF">
					<IconDescription />
					<span className="pdfLabel">PDF</span>
				</div>
			}
			{metadata &&
				<div className="cardMetas">
					{metadata.map(function(d, idx) {
						return (
							<div className="cardMeta" key={`${d.metaLabel}-${d.metaValue}-${idx}`}>
								<div className="cardMetaLabel">{d.metaLabel}</div>
								<div className="cardMetaValue">{d.metaValue}</div>
							</div>
						)
					})}
				</div>
			}
			{footer &&
				<div className="cardFooter">
					<div className="cardFooterMeta">
						<a className="cardFooterMetaUser" href={footer.authorURL}>
							<div className="cardFooterMetaUserImage">
								<img src={footer.authorImageURL} alt="" width={footer.authorImageWidth} height={footer.authorImageHeight} />
							</div>
							<span className="cardFooterMetaUserName">{footer.authorName}</span>
						</a>
						<div className="cardFooterMetaPosted"><TimeAgo date={footer.postedDate} /></div>
					</div>
					<div className="cardFooterActions">
					</div>
				</div>
			}
			{counts &&
				<div className="cardCounts">
					{counts.map((count) => {
						return (
							<div key={count.label}>
								<div className="countLabel">{count.label}</div>
								<div className="countValue">{count.value.toLocaleString()}</div>

							</div>
						)
					})}
				</div>
			}
		</React.Fragment>
	);

	if (to && to.length) {
		return (
			<Link className={_classes.join(' ')} to={to}>
				{cardContent}
			</Link>
		);
	}

	return (
		<a
			className={_classes.join(' ')}
			href={href}
			rel="noopener noreferrer"
		>
			{cardContent}
		</a>
	);
};

Card.propTypes = {
	to: PropTypes.string,
	href: PropTypes.string,
	title: PropTypes.string,
	textShort: PropTypes.string,
	textLong: PropTypes.string,
	assetImageSrc: PropTypes.string,
	assetImageWidth: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number
	]),
	assetImageHeight: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number
	]),
	assetImageAlt: PropTypes.string,
	assetAudioSrc: PropTypes.string,
	assetVideoSrc: PropTypes.string,
	asset3DSrc: PropTypes.string,
	metadata: PropTypes.arrayOf(PropTypes.shape({
		metaLabel: PropTypes.string,
		metaValue: PropTypes.string,
	})),
	footer: PropTypes.shape({
		authorURL: PropTypes.string,
		authorImageURL: PropTypes.string,
		authorImageWidth: PropTypes.string,
		authorImageHeight: PropTypes.string,
		authorName: PropTypes.string,
		postedDate: PropTypes.instanceOf(Date),
	}),
	horizontal: PropTypes.bool,
	loading: PropTypes.bool,
	children: PropTypes.node,
};

export default Card;
