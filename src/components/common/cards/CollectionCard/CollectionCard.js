import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import prune from 'underscore.string/prune';

import { mockFiles } from '../../../../mocks/files';



const CollectionCard = ({
	type, _id, slug, loading, title, description, itemsCount, className, files,
}) => {
	let itemUrl = `/${type}/${_id}/${slug}`;

	// hack for Alexandria demo
	itemUrl = "//api.alexandria.staging.archimedes.digital/read/urn:cts:greekLit:tlg0548.tlg001.perseus-grc2";

	const _classes = ['CollectionCard'];

	if (loading) {
		_classes.push('-loading');
	}

	if (className) {
		_classes.push(className);
	}

	let file;
	if (files) {
		let _files = files.filter((f)=>{
			const fileType = f.type || '';
			return  fileType.slice(0, fileType.indexOf('/')) === 'image';
		}).map((a) => ({sort: Math.random(), value: a}))
		  .sort((a, b) => a.sort - b.sort)
		  .map((a) => a.value);
		if (_files) {
			file = _files[0];
		}

	}
	return (
		<div
			className={_classes.join(' ')}
		>
			<Link
				to={itemUrl}
			>


				{file ?
					<div
						className="CollectionThumbnail"
						style={{
							backgroundImage: `url('//iiif.orphe.us/${file.name}/square/350,/0/default.jpg')`,
						}}
					>
					</div>
					:
					<div className="CollectionThumbnail"></div>
			 }


				<div className="CollectionCardInner">
					<p
						className="CollectionCardTitle"
					>
						{title}
					</p>
					{description && <span
						className="CollectionCardDescription"
						variant="caption"
					>
						{prune(description, 100, '')}
					</span>}
					{itemsCount ?
						<span
							className="CollectionCardMeta"
							variant="caption"
						>
							{itemsCount}
						</span>
						: ""}
				</div>
			</Link>
		</div>
	);
};

CollectionCard.propTypes = {
	_id: PropTypes.string,
	title: PropTypes.string,
	slug: PropTypes.string,
	itemsCount: PropTypes.number,
	files: PropTypes.array,
	description: PropTypes.string,
	author: PropTypes.string,
	type: PropTypes.string,
	loading: PropTypes.bool,
	compact: PropTypes.bool,
};

CollectionCard.defaultProps = {
	type: 'items',
};

export default CollectionCard;
