import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import { Link } from 'react-router-dom';
import { compose } from 'react-apollo';
import { Grid, Row, Col } from 'react-bootstrap';


// graphql
import projectsQuery from '../../../modules/settings/graphql/queries/detail';


import './Footer.css';


class Footer extends React.Component {

	render() {
		const { projectsQuery } = this.props;
		let project = null;

		if (projectsQuery && projectsQuery.project) {
			project = projectsQuery.loading ? null : projectsQuery.project;
		}

		const now = new Date();
		const year = now.getFullYear();

		return (
			<footer className="block-shadow">
				<Grid>
					<Row>
						<div className="footer-nav-row">
							<div className="footer-nav-links" role="navigation">
								<div>
									<Link to="/commentary">
										<FlatButton
											label="Commentary"
										/>
									</Link>
									<Link to="/commenters">
										<FlatButton
											label="Commentators"
										/>
									</Link>
									<Link to="/words">
										<FlatButton
											label="Words"
										/>
									</Link>
									<Link to="/ideas">
										<FlatButton
											label="Ideas"
										/>
									</Link>
									<Link to={'/about'}>
										<FlatButton
											label="About"
										/>
									</Link>
								</div>
							</div>
						</div>
					</Row>
					<Row>
						<div className="footerLogoRow clearfix">
							<Col md={5}>
								<h1 className="logo">{project ? project.title : ''}</h1>
							</Col>

							<Col md={2}>
								<a href="http://chs.harvard.edu" target="_blank" rel="noopener noreferrer">
									<img
										className="site-logo"
										src="/images/center_for_hellenic_studies_lighthouse.png"
										role="presentation"
										alt="The Center for Hellenic Studies"
									/>
								</a>
							</Col>

							<Col md={5}>
								<p className="lead">
									For more information about the Commentary or general media inquiries,
									please contact &nbsp;
									<a href="mailto:contact@chs.harvard.edu">
										contact@chs.harvard.edu
									</a>.
								</p>

								<p className="lead">
									This website is provided by <a href="http://chs.harvard.edu" target="_blank" rel="noopener noreferrer">
										The Center for Hellenic Studies
									</a>.
								</p>
							</Col>
						</div>
					</Row>
					<Row>
						<Col md={8} mdOffset={2}>
							<p className="fade-1-4 copyright">
								Copyright {year} The Center for Hellenic Studies.
								See our <a href="/terms">terms and privacy policy</a>, <a href="https://gdpr.harvard.edu/eeaprivacydisclosures" target="_blank" rel="noopener noreferrer">EU/EEA Privacy Disclosures</a>, and <a href="https://accessibility.huit.harvard.edu/digital-accessibility-policy">Digital Accessibility</a>.
							</p>
						</Col>
					</Row>
				</Grid>
			</footer>

		);
	}
}

Footer.propTypes = {
	projectQuery: PropTypes.object,
};

export default Footer;
