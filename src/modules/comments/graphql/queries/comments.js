import { gql, graphql } from 'react-apollo';

// lib
import getCommentsQuery from '../../lib/getCommentsQuery';
import getCurrentProjectHostname from '../../../../lib/getCurrentProjectHostname';

const query = gql`
query commentsQuery($hostname: String $urnSearch: String, $commenterNameSearch: String, $searchTerm: String) {
  project(hostname: $hostname) {
    id
    hostname
    title

    comments(urnSearch: $urnSearch, commenterNameSearch: $commenterNameSearch, searchTerm: $searchTerm){
      id
      urn
      privacy
      tags {
        id
        name
      }
      commenters {
        id
        username
        firstName
        lastName
        picture
      }
      revisions {
        id
        title
        createdAt
        updatedAt
        text
      }
    }

  }

}
`;

const commentsQuery = graphql(query, {
	name: 'commentsQuery',
	options: (params) => {
		return ({
			variables: {
				hostname: getCurrentProjectHostname().split('.')[0],
				urnSearch: "urn:cts:greekLit:tlg0012.tlg001.perseus-grc2:1.1-1.30", // params.urn,
				commentersNameSearch: params.commenterNameSearch,
				searchTerm: params.searchTerm,
			}
		});
	}
});

export default commentsQuery;
