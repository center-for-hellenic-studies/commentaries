import React from 'react';
import { Route } from 'react-router';

import CommentaryLayout from '../layouts/CommentaryLayout';
import EditorLayout from '../layouts/EditorLayout';


const AlexEditing = () => {
	return window.location = "http://homer.beta.newalexandria.info/read/urn:cts:greekLit:tlg0012.tlg001.perseus-grc2:1.1-1.30";
};



const addCommentRoute = (
	<Route
		exact
		roles={['commenter', 'editor', 'admin']}
		path="/commentary/create"
		component={AlexEditing}
	/>
);

const editCommentRoute = (
	<Route
		exact
		roles={['commenter', 'editor', 'admin']}
		path="/commentary/edit/:id?"
		component={AlexEditing}
	/>
);

const commentaryRoute = (
	<Route
		exact
		path="/commentary/:urn?"
		component={CommentaryLayout}
	/>
);

export { addCommentRoute, editCommentRoute, commentaryRoute };
