import _ from 'underscore';

import getCommentGroupId from './getCommentGroupId';


/**
 * Add unique commenters to the comment group
 */
const addCommetersToCommentGroup = (comment, commenters = {}) => {
	if (comment.commenters) {
		comment.commenters.forEach(function(_commenter) {
			commenters[_commenter._id] = _commenter;
		});
	}
	return commenters;
}

/**
 * parse a list of comments into comment groups based on lemmaCitation
 */
const parseCommentsToCommentGroups = (comments) => {
	const commentGroups = [];

	// Make comment groups from comments
	let isInCommentGroup = false;
	comments.forEach((comment) => {
		isInCommentGroup = false;
		if (comment.urn) {
			commentGroups.forEach((commentGroup) => {
				if (comment.urn === commentGroup.urn) {
					isInCommentGroup = true;
					commentGroup.comments.push(comment);
				}
			});

			if (!isInCommentGroup) {
				commentGroups.push({
					selectedLemmaEdition: {
						lines: [],
					},
					comments: [comment],
					urn: comment.urn,
				});
			}

		} else if (process.env.NODE_ENV === 'development') {
			console.error(`Review comment ${comment._id} metadata`);
		}
	});

	// Parse metadata for comment group
	commentGroups.forEach((commentGroup) => {
		// commentgroup id
		commentGroup._id = getCommentGroupId(commentGroup);
		// commenters
		commentGroup.commenters = {};
		commentGroup.comments.forEach((comment) => {
			addCommetersToCommentGroup(comment, commentGroup.commenters);
		});
	});

	return commentGroups;
};

export default parseCommentsToCommentGroups;
