import React from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import { withRouter } from 'react-router';
import qs from 'qs-lite';


const getLabel = (keyword) => {
	let label = keyword.name;
	if (keyword.isMentionedInLemma) label = `[ ${label} ]`;
	return label;
};

const goToSearchTerm = (keyword, history) => {
	const queryParams = qs.parse(window.location.search.replace('?', ''));
	queryParams.page = 0;
	queryParams.keywords = keyword.name;

	// update route
	const urlParams = qs.stringify(queryParams);
	history.push(`/commentary/?${urlParams}`);
};

const CommentKeywordsContainer = props => (
	<div className="comment-keywords-container">
		{props.keywords.map((keyword, i) => (
			<RaisedButton
				key={`${i}-${keyword.id}`}
				className="comment-keyword paper-shadow"
				onClick={goToSearchTerm.bind(this, keyword, props.history)}
				data-id={keyword.id}
				label={getLabel(keyword)}
			/>
		))}
	</div>
);

CommentKeywordsContainer.propTypes = {
	keywords: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
	})).isRequired,
};

CommentKeywordsContainer.defaultProps = {
	keywords: [],
};

export default withRouter(CommentKeywordsContainer);
