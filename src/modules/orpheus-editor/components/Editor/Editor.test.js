/**
 * @prettier
 */

import React from 'react';
import draftJs, { ContentState, EditorState, convertFromRaw } from 'draft-js';
import { shallow } from 'enzyme';

import OrpheusEditor, {
	DEFAULT_CUSTOM_KEY_COMMANDS as customKeyCommands,
} from './Editor';

describe('OrpheusEditor', () => {
	it('renders correctly', () => {
		const wrapper = shallow(<OrpheusEditor />);

		expect(wrapper).toBeDefined();
	});

	describe('FormattingTooltip', () => {
		it('renders FormattingTooltip if `readOnly == false`', () => {
			const wrapper = shallow(
				<OrpheusEditor editorState={EditorState.createEmpty()} />
			);

			expect(wrapper.find('FormattingTooltip')).toHaveLength(1);
		});

		it('does not render FormattingTooltip if `readOnly == true`', () => {
			const wrapper = shallow(
				<OrpheusEditor editorState={EditorState.createEmpty()} readOnly />
			);

			expect(wrapper.find('FormattingTooltip')).toHaveLength(0);
		});
	});

	describe('handleBeforeInput()', () => {
		it('returns `handled` if the current block starts an ordered list', () => {
			const wrapper = shallow(
				<OrpheusEditor editorState={EditorState.createEmpty()} />
			);

			const editor = wrapper.instance();
			const content = ContentState.createFromText('1.');
			const editorState = EditorState.createWithContent(content);

			expect(editor.handleBeforeInput('', editorState)).toEqual('handled');
		});

		it('returns `handled` if the current block starts an unordered list with *', () => {
			const wrapper = shallow(
				<OrpheusEditor editorState={EditorState.createEmpty()} />
			);

			const editor = wrapper.instance();
			const content = ContentState.createFromText('* ');
			const editorState = EditorState.createWithContent(content);

			expect(editor.handleBeforeInput('', editorState)).toEqual('handled');
		});

		it('returns `handled` if the current block starts an unordered list with -', () => {
			const wrapper = shallow(
				<OrpheusEditor editorState={EditorState.createEmpty()} />
			);

			const editor = wrapper.instance();
			const content = ContentState.createFromText('- ');
			const editorState = EditorState.createWithContent(content);

			expect(editor.handleBeforeInput('', editorState)).toEqual('handled');
		});

		it('returns `not-handled` if the current block does not start a list', () => {
			const wrapper = shallow(
				<OrpheusEditor editorState={EditorState.createEmpty()} />
			);

			const editor = wrapper.instance();
			const content = ContentState.createFromText('foo');
			const editorState = EditorState.createWithContent(content);

			expect(editor.handleBeforeInput('', editorState)).toEqual('not-handled');
		});
	});

	describe('keyBindingFn()', () => {
		const _info = console.info;
		const makeContent = (type, text = '') => ({
			blocks: [
				{
					entityRanges: [],
					text,
					type,
				},
			],
			entityMap: {},
		});

		beforeAll(() => {
			console.info = noop;
		});

		afterAll(() => {
			// turn logging back on
			console.info = _info;
		});

		describe('with empty block', () => {
			it('returns `handle-blockquote-backspace` when in blockquote mode', () => {
				const content = makeContent('blockquote');
				const editorState = EditorState.createWithContent(
					convertFromRaw(content)
				);
				const wrapper = shallow(<OrpheusEditor editorState={editorState} />);
				const result = wrapper.instance().keyBindingFn({ keyCode: 8 });

				expect(result).toEqual('handle-blockquote-backspace');
			});

			it('returns `handle-list-mode-backspace` when a list', () => {
				const content = makeContent('ordered-list-item');
				const editorState = EditorState.createWithContent(
					convertFromRaw(content)
				);
				const wrapper = shallow(<OrpheusEditor editorState={editorState} />);
				const result = wrapper.instance().keyBindingFn({ keyCode: 8 });

				expect(result).toEqual('handle-list-mode-backspace');
			});

			it('returns `handle-header-mode-backspace` when a header', () => {
				const content = makeContent('header-one');
				const editorState = EditorState.createWithContent(
					convertFromRaw(content)
				);
				const wrapper = shallow(<OrpheusEditor editorState={editorState} />);
				const result = wrapper.instance().keyBindingFn({ keyCode: 8 });

				expect(result).toEqual('handle-header-mode-backspace');
			});
		});

		it('calls a customKeyCommand if the command modifier is pressed and the command exists', () => {
			const content = makeContent('unstyled', 'foo');
			const editorState = EditorState.createWithContent(
				convertFromRaw(content)
			);
			const wrapper = shallow(<OrpheusEditor editorState={editorState} />);
			const result = wrapper.instance().keyBindingFn({
				ctrlKey: true,
				keyCode: 83,
			});

			expect(result).toEqual('handle-cmd-s');
		});

		it('calls a customKeyCommand alt-command if the command modifier is pressed with alt and the command exists', () => {
			const content = makeContent('unstyled', 'foo');
			const editorState = EditorState.createWithContent(
				convertFromRaw(content)
			);
			const wrapper = shallow(<OrpheusEditor editorState={editorState} />);
			const result = wrapper.instance().keyBindingFn({
				altKey: true,
				ctrlKey: true,
				keyCode: 49,
				preventDefault: noop,
			});

			expect(result).toEqual('handle-cmd-alt-1');
		});

		it('falls through to `getDefaultKeyBinding()` if no other match', () => {
			const _getDefaultKeyBinding = draftJs.getDefaultKeyBinding;

			draftJs.getDefaultKeyBinding = jest.fn();

			const content = makeContent('unstyled', 'foo');
			const editorState = EditorState.createWithContent(
				convertFromRaw(content)
			);
			const wrapper = shallow(<OrpheusEditor editorState={editorState} />);
			const e = {
				keyCode: 74, // j
			};

			wrapper.instance().keyBindingFn(e);

			expect(draftJs.getDefaultKeyBinding).toHaveBeenCalledTimes(1);
			expect(draftJs.getDefaultKeyBinding).toHaveBeenCalledWith(e);

			draftJs.getDefaultKeyBinding = _getDefaultKeyBinding;
		});
	});
});
