/**
 * @prettier
 */

import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import { mount, shallow } from 'enzyme';

import ItemBlock from './ItemBlock';
import client from '../../../../../middleware/apolloClient';
import configureStore from '../../../../../store/configureStore';

describe('ItemBlock', () => {
	it('renders', () => {
		const wrapper = shallow(
			<Provider store={configureStore()}>
				<ItemBlock block={{ getData: noop }} />
			</Provider>
		);

		expect(wrapper).toBeDefined();
	});

	describe('renderRemoveButton()', () => {
		const getData = () => ({
			toJS: () => ({
				itemId: 'itemId',
			}),
		});

		it('renders if the user is logged in and on the edit page', () => {
			window.history.pushState({}, 'renderRemoveButton()', '/edit/');

			const wrapper = mount(
				<ApolloProvider client={client}>
					<Provider store={configureStore({ auth: { userId: 'userId' } })}>
						<ItemBlock block={{ getData }} />
					</Provider>
				</ApolloProvider>
			);

			expect(wrapper.find('.itemBlockRemove')).toHaveLength(1);
		});

		it('renders if the user is logged in and on the create page', () => {
			window.history.pushState({}, 'renderRemoveButton()', '/create/');

			const wrapper = mount(
				<ApolloProvider client={client}>
					<Provider store={configureStore({ auth: { userId: 'userId' } })}>
						<ItemBlock block={{ getData }} />
					</Provider>
				</ApolloProvider>
			);

			expect(wrapper.find('.itemBlockRemove')).toHaveLength(1);
		});

		it('does not render if the user is not logged in', () => {
			window.history.pushState({}, 'renderRemoveButton()', '/edit/');

			const wrapper = mount(
				<ApolloProvider client={client}>
					<Provider store={configureStore()}>
						<ItemBlock block={{ getData }} />
					</Provider>
				</ApolloProvider>
			);

			expect(wrapper.find('.itemBlockRemove')).toHaveLength(0);
		});

		it('does not render if not on the item edit page', () => {
			window.history.pushState({}, 'renderRemoveButton()', '/not-edit/');

			const wrapper = mount(
				<ApolloProvider client={client}>
					<Provider store={configureStore({ auth: { userId: 'userId' } })}>
						<ItemBlock block={{ getData }} />
					</Provider>
				</ApolloProvider>
			);

			expect(wrapper.find('.itemBlockRemove')).toHaveLength(0);
		});
	});
});
