/**
 * @prettier
 */

import React from 'react';
import { convertFromRaw, genKey } from 'draft-js';
import { shallow } from 'enzyme';

import EmbedBlock from '../EmbedBlock';
import ImageBlock from '../ImageBlock';
import ItemBlock from '../ItemBlock';
import MediaBlock from './MediaBlock';

describe('MediaBlock', () => {
	it('renders correctly', () => {
		const rawContent = {
			blocks: [
				{
					entityRanges: [{ key: 'first', length: 1, offset: 0 }],
					key: genKey(),
					text: ' ',
					type: 'atomic',
				},
			],
			entityMap: {
				first: {
					data: {
						url: 'https://placekitten.com',
					},
					mutability: 'IMMUTABLE',
					type: 'EMBED',
				},
			},
		};

		const content = convertFromRaw(rawContent);
		const block = content.getFirstBlock();
		const wrapper = shallow(
			<MediaBlock block={block} contentState={content} />
		);
		expect(wrapper).toBeDefined();
	});

	describe('EmbedBlock', () => {
		it('renders an EmbedBlock for entityType `EMBED`', () => {
			const rawContent = {
				blocks: [
					{
						entityRanges: [{ key: 'first', length: 1, offset: 0 }],
						key: genKey(),
						text: ' ',
						type: 'atomic',
					},
				],
				entityMap: {
					first: {
						data: {
							url: 'https://placekitten.com',
						},
						mutability: 'IMMUTABLE',
						type: 'EMBED',
					},
				},
			};

			const content = convertFromRaw(rawContent);
			const block = content.getFirstBlock();
			const wrapper = shallow(
				<MediaBlock block={block} contentState={content} />
			);
			expect(wrapper.find(EmbedBlock)).toHaveLength(1);
		});
	});

	describe('ImageBlock', () => {
		it('renders an ImageBlock for entityType `IMAGE`', () => {
			const rawContent = {
				blocks: [
					{
						entityRanges: [{ key: 'first', length: 1, offset: 0 }],
						key: genKey(),
						text: ' ',
						type: 'atomic',
					},
				],
				entityMap: {
					first: {
						data: {
							src: 'https://placekitten.com/40/40',
						},
						mutability: 'IMMUTABLE',
						type: 'IMAGE',
					},
				},
			};

			const content = convertFromRaw(rawContent);
			const block = content.getFirstBlock();
			const wrapper = shallow(
				<MediaBlock block={block} contentState={content} />
			);
			expect(wrapper.find(ImageBlock)).toHaveLength(1);
		});
	});

	describe('ItemBlock', () => {
		it('renders an ItemBlock for entityType `ITEM`', () => {
			const rawContent = {
				blocks: [
					{
						entityRanges: [{ key: 'first', length: 1, offset: 0 }],
						key: genKey(),
						text: ' ',
						type: 'atomic',
					},
				],
				entityMap: {
					first: {
						data: {
							itemId: 'test-item-id',
						},
						mutability: 'IMMUTABLE',
						type: 'ITEM',
					},
				},
			};

			const content = convertFromRaw(rawContent);
			const block = content.getFirstBlock();
			const wrapper = shallow(
				<MediaBlock block={block} contentState={content} />
			);
			expect(wrapper.find(ItemBlock)).toHaveLength(1);
		});
	});

	describe('attemptRescue()', () => {
		// note that this is not the correct behavior -- we're testing an edge case
		// for legacy support, and we should remove this functionality when possible.
		it("attempts to use the contentBlock's data if there is no entity", () => {
			const rawContent = {
				blocks: [
					{
						data: {
							src: 'https://placekitten.com/40/40',
						},
						entityRanges: [],
						key: genKey(),
						text: ' ',
						type: 'image',
					},
				],
				entityMap: {},
			};

			// silence conosle warnings
			const warn = console.warn;
			console.warn = () => {};

			const content = convertFromRaw(rawContent);
			const block = content.getFirstBlock();
			const wrapper = shallow(
				<MediaBlock block={block} contentState={content} />
			);
			expect(wrapper.find(ImageBlock)).toHaveLength(1);

			// bring back console warnings
			console.warn = warn;
		});
	});
});
