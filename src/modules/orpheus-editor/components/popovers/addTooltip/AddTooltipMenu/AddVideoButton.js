/**
 * @prettier
 */

import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import { EditorState } from 'draft-js';

// components
import AddTooltipMenuItemButton from '../AddTooltipMenuItemButton';

import insertEntity from '../../../../lib/insertEntity';

function UrlEntry({ addEntity, reset }) {
	const input = useRef({});
	const [src, setSrc] = useState('');
	const handleChange = evt => setSrc(evt.target.value);
	const handleClick = evt => {
		evt.preventDefault();
		evt.stopPropagation();

		input.current.focus();
	};
	const handleSubmit = evt => {
		evt.preventDefault();
		evt.stopPropagation();

		addEntity(src);
	};
	const handleKeyPress = evt => {
		if (evt.which === 13 /*Enter*/) {
			handleSubmit(evt);
		} else if (evt.which === 27 /* Esc */) {
			reset();
		}
	};

	return (
		<div
			onClick={handleClick}
			style={{
				display: 'flex',
				height: '100%',
				minHeight: 40,
				minWidth: 400,
				padding: 20,
				width: '100%',
			}}
		>
			<input
				autoFocus
				className="linkTextInput"
				onChange={handleChange}
				onKeyPress={handleKeyPress}
				ref={input}
				style={{ minWidth: 400, padding: 10 }}
				value={src}
			/>
		</div>
	);
}

class AddVideoButton extends React.Component {
	static propTypes = {
		closeMenu: PropTypes.func.isRequired,
		description: PropTypes.string.isRequired,
		icon: PropTypes.element.isRequired,
		editorState: PropTypes.instanceOf(EditorState).isRequired,
		setEditorState: PropTypes.func.isRequired,
		setParentContent: PropTypes.func.isRequired,
		videoType: PropTypes.oneOf(['vimeo', 'youtube']).isRequired
	};

	handleAddUrl = src => {
		const {
			closeMenu,
			editorState,
			setEditorState,
			setParentContent,
			videoType
		} = this.props;

		const newEditorState = insertEntity(editorState, {
			entityType: 'VIDEO',
			entityData: { src, videoType },
		});
		setEditorState(newEditorState);

		window.requestAnimationFrame(() => setParentContent(null));
		window.requestAnimationFrame(closeMenu);
	};

	handleClick = _evt => {
		const { setParentContent } = this.props;

		setParentContent(
			<UrlEntry addEntity={this.handleAddUrl} reset={setParentContent} />
		);
	};

	render() {
		const { description, icon } = this.props;

		return (
			<AddTooltipMenuItemButton
				className="AddTooltipMenuItemButton"
				onClick={this.handleClick}
			>
				{icon}
				<span>{description}</span>
			</AddTooltipMenuItemButton>
		);
	}
}

export default AddVideoButton;
