/**
 * @prettier
 */
import React from 'react';
import { shallow } from 'enzyme';
import { EditorState } from 'draft-js';

import AddTooltip from './AddTooltip';
import AddTooltipButton from '../AddTooltipButton';

const fakeClick = {
	preventDefault: () => {},
	stopPropagation: () => {},
};

const fakeClickOnMenu = {
	...fakeClick,
	target: {
		parentNode: {
			className: 'addTooltipMenu',
		},
	},
};

describe('AddTooltip', () => {
	it('renders correctly', () => {
		const wrapper = shallow(
			<AddTooltip
				editorRef={React.createRef()}
				editorState={EditorState.createEmpty()}
			/>
		);
		expect(wrapper).toBeDefined();
	});

	describe('toggleAddTooltipMenu()', () => {
		it('shows the AddTooltipMenu when triggered once', () => {
			const wrapper = shallow(
				<AddTooltip
					editorRef={React.createRef()}
					editorState={EditorState.createEmpty()}
				/>
			);

			expect(wrapper.state('menuVisible')).toBeFalsy();

			wrapper.find(AddTooltipButton).simulate('click', fakeClick);
			wrapper.update();

			expect(wrapper.state('menuVisible')).toBeTruthy();
		});

		it('shows then hides the AddTooltipMenu when triggered twice', () => {
			const wrapper = shallow(
				<AddTooltip
					editorRef={React.createRef()}
					editorState={EditorState.createEmpty()}
				/>
			);

			expect(wrapper.state('menuVisible')).toBeFalsy();

			const button = wrapper.find(AddTooltipButton);

			button.simulate('click', fakeClick);
			wrapper.update();

			expect(wrapper.state('menuVisible')).toBeTruthy();

			button.simulate('click', fakeClick);
			wrapper.update();

			expect(wrapper.state('menuVisible')).toBeFalsy();
		});
	});

	describe('collapseAddTooltipMenu()', () => {
		it('does not collapse the menu if the click is on the AddTooltipMenu', () => {
			const wrapper = shallow(
				<AddTooltip
					editorRef={React.createRef()}
					editorState={EditorState.createEmpty()}
				/>
			);

			expect(wrapper.state('menuVisible')).toBeFalsy();

			wrapper.find(AddTooltipButton).simulate('click', fakeClick);
			wrapper.update();

			expect(wrapper.state('menuVisible')).toBeTruthy();

			wrapper.simulate('click', fakeClickOnMenu);
			wrapper.update();

			expect(wrapper.state('menuVisible')).toBeTruthy();
		});
	});
});
