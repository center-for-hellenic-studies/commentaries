/**
 * @prettier
 */

import React from 'react';
import { EditorState } from 'draft-js';
import { shallow } from 'enzyme';

import FormattingTooltipLink from './FormattingTooltipLink';

describe('FormattingTooltipLink', () => {
	it('renders correctly', () => {
		const wrapper = shallow(
			<FormattingTooltipLink
				editorState={EditorState.createEmpty()}
				overrideContent={noop}
			/>
		);
		expect(wrapper).toBeDefined();
	});
});
