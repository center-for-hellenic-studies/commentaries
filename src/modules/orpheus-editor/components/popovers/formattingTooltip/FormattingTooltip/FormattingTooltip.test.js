/**
 * @prettier
 */

import React from 'react';
import { EditorState } from 'draft-js';
import { mount, shallow } from 'enzyme';

import FormattingTooltip from './FormattingTooltip';

describe('FormattingTooltip', () => {
	it('renders correctly', () => {
		const wrapper = shallow(
			<FormattingTooltip
				editorRef={React.createRef()}
				editorState={EditorState.createEmpty()}
				formattingTooltipContent={null}
				setFormattingTooltipContent={noop}
				setEditorState={noop}
			/>
		);
		expect(wrapper).toBeDefined();
	});

	describe('overrideContent', () => {
		const editorRef = React.createRef();
		const editorState = EditorState.createEmpty();

		it('renders <FormattingTooltipButtons /> by default', () => {
			const wrapper = mount(
				<FormattingTooltip
					editorRef={editorRef}
					editorState={editorState}
					setFormattingTooltipContent={noop}
					setEditorState={noop}
				/>
			);

			expect(wrapper.find('FormattingTooltipButtons')).toHaveLength(1);
			wrapper.unmount();
		});

		it('renders the provided component from `setFormattingTooltipContent`', () => {
			const TestComponent = () => (
				<div className="formattingTooltip--testDiv" />
			);

			const wrapper = shallow(
				<FormattingTooltip
					editorRef={editorRef}
					editorState={editorState}
					formattingTooltipContent={TestComponent}
					setFormattingTooltipContent={noop}
					setEditorState={noop}
				/>
			);

			expect(wrapper.find('TestComponent')).toBeTruthy();
		});
	});
});
