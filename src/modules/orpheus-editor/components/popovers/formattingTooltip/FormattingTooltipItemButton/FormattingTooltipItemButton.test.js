/**
 * @prettier
 */

import React from 'react';
import { shallow } from 'enzyme';

import TooltipItemButton from './FormattingTooltipItemButton';

const noop = () => {};

describe('TooltipItemButton', () => {
	it('renders correctly', () => {
		const wrapper = shallow(
			<TooltipItemButton onClick={noop}>
				<small>test</small>
			</TooltipItemButton>
		);
		expect(wrapper).toBeDefined();
	});
});
