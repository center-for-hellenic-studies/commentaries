/**
 * @prettier
 */

import React from 'react';
import { convertFromRaw, genKey } from 'draft-js';
import { shallow } from 'enzyme';

import { Link } from './Link';

describe('Link', () => {
	const rawContent = {
		blocks: [
			{
				entityRanges: [{ key: 'first', length: 1, offset: 0 }],
				key: genKey(),
				text: ' ',
				type: 'atomic',
			},
		],
		entityMap: {
			first: {
				data: {
					url: 'test.url',
				},
				mutability: 'IMMUTABLE',
				type: 'LINK',
			},
		},
	};
	it('renders correctly', () => {
		const contentState = convertFromRaw(rawContent);
		const entityKey = contentState.getLastCreatedEntityKey();

		const wrapper = shallow(
			<Link contentState={contentState} entityKey={entityKey} />
		);
		expect(wrapper).toBeDefined();
	});
});
