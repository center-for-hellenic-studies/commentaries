import React from 'react';
import { convertFromRaw, genKey } from 'draft-js';
import { shallow } from 'enzyme';

import { Gloss } from './Gloss';

describe('Gloss', () => {
	const rawContent = {
		blocks: [
			{
				entityRanges: [{ key: 'gloss', length: 10, offset: 0 }],
				key: genKey(),
				text: ' ',
				type: 'atomic',
			},
		],
		entityMap: {
			gloss: {
				data: {
					glossText: 'test-gloss-text',
				},
				mutability: 'IMMUTABLE',
				type: 'GLOSS',
			},
		},
	};

	it('renders correctly', () => {
		const contentState = convertFromRaw(rawContent);
		const entityKey = contentState.getLastCreatedEntityKey();

		const wrapper = shallow(
			<Gloss contentState={contentState} entityKey={entityKey}>
				<p>Testing glosses</p>
			</Gloss>
		);
		expect(wrapper).toBeDefined();
	});
});
