import ImageDecorator, { Image, findImageEntities } from './Image';
export { Image, findImageEntities };
export default ImageDecorator;
