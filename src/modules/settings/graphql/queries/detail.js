import { gql, graphql } from 'react-apollo';

import getCurrentProjectHostname from '../../../../lib/getCurrentProjectHostname';


const projectByHostname = gql`
query projectsQuery($hostname: String!) {
  project(hostname: $hostname) {
    id
    hostname
    title
    aboutHtml
    privacy
  }
}
`;

const projectQueryByHostname = graphql(projectByHostname, {
	options: () => ({
		variables: {
			hostname: getCurrentProjectHostname(),
		},
	}),
	name: 'projectQueryByHostname'
});

export default projectQueryByHostname;
