import { gql, graphql } from 'react-apollo';

const query = gql`
query projectsQuery {
  projects {
    _id
    hostname
    title
    aboutHtml
    privacy
  }
}
`;

const projectsQuery = graphql(query, {
	name: 'projectsQuery'
});

export default projectsQuery;
