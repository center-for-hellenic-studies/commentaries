import React from 'react';
import { compose } from 'react-apollo';

import Filters from '../../../../components/common/Filters';

import itemListQuery from '../../graphql/queries/list';

class CommentariesFiltersContainer extends React.Component {
	constructor(props) {
		super(props);
	}

	render () {
		let filters = this.props.filters || [];
		let relevantFilters = this.props.relevantFilters || [];
		// if (
		// 	this.props.itemListQuery
	    // && this.props.itemListQuery.TEXT_works
		// ) {
		// 			// [{name, type, values}]
		// 	let filterLookup = {}

		// 	this.props.itemListQuery.TEXT_works.forEach((text) => {
		// 		filterLookup.language = filterLookup.language || {
		// 			name: 'Language',
		// 			type: 'text',
		// 			values: []
		// 		}
		// 		if (!filterLookup.language.values.includes(text.language.title)) {
		// 			 filterLookup.language.values.push(text.language.title);
		// 		}

		// 		filterLookup.work_type = filterLookup.work_type || {
		// 			name: 'Work Type',
		// 			type: 'text',
		// 			values: []
		// 		}
		// 		if (text.work_type && text.work_type.length > 0 && !filterLookup.work_type.values.includes(text.work_type)) {
		// 			 filterLookup.work_type.values.push(text.work_type);
		// 		}

		// 		filterLookup.structure = filterLookup.structure || {
		// 			name: 'Structure',
		// 			type: 'text',
		// 			values: []
		// 		}
		// 		if (text.structure && text.structure.length > 0 && !filterLookup.structure.values.includes(text.structure)) {
		// 			 filterLookup.structure.values.push(text.structure);
		// 		}
		// 	})

		// 	Object.keys(filterLookup).forEach((filter_name) => {
		// 		filters.push(filterLookup[filter_name])
		// 	})
		// }


		return (
			<div>
				{this.props.loading ? (
					<Filters
						filters={filters}
						loading
					/>
				) : (
					<Filters
						filters={filters}
						relevantFilters={relevantFilters}
					/>
				)}
			</div>
		);
	}
}


export default CommentariesFiltersContainer;
/**
export default compose(
	itemListQuery
)(ArchiveFiltersContainer);
*/
