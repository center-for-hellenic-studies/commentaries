import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';


const query = gql`
	query itemQuery($hostname: String, $id: String) {
		project(hostname: $hostname) {
	    _id
			title
			userIsAdmin
			tags
			collections {
				_id
				title
			}
			item(_id: $id) {
				_id
				title
				slug
				description
				private
				projectId
				tags
				collectionId
				metadata {
					type
					label
					value
				}

				files {
					_id
					name
					title
					type
					path
					annotations {
						x
						y
						content
						order
					}
				}

				commentsCount
				comments {
					_id
					userId
					itemId
					content
					updatedAt
				}

				manifest {
					_id
				}
			}
		}
	}
`;

const itemQuery = graphql(query, {
	name: 'itemQuery',
	options: (props) => {
		let id;

		if (props.match && props.match.params) {
			id = props.match.params.id;
		}

		return {
			variables: {
				hostname: getCurrentArchiveHostname(props.hostname),
				id,
			},
		};
	},
});

export default itemQuery;
