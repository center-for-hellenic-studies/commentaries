import React from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Sticky from 'react-stickynode';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import qs from 'qs-lite';

import List from '../../../../components/common/lists/List';
import CustomTheme from '../../../../lib/muiTheme';
import Filters from '../../../../components/common/Filters';
import CommentariesFiltersContainer from '../../containers/CommentariesFiltersContainer';



class CommentariesFacetedCards extends React.Component {
	constructor(props) {
		super(props);
	}

	render () {
		const query = qs.parse(window.location.search.replace('?', ''));

		const _classes = this.props.classes || [];

		_classes.push('facetedCards');

		if (this.props.loading) {
			_classes.push('-loading');
		}

		const commentaries = [{
			title: "A Homer Commentary in Progress",
			textShort: "Gregory Nagy, Leonard Muellner, Doug Frame, and 10 others",
			description: "An evolving, collaborative commentary based on the cumulative research of Milman Parry and Albert Lord, who created a new way of thinking about Homeric poetry. Based on the research of Milman Parry and Albert Lord, the Homeric poems are the product of a performance tradition that stretches back to the period of Indo-European community, the 3rd Millennium BCE. Breaking from conventional Homer research, our approach to Homeric diction is profoundly anthropological and, if we can help it, never judgmental",
			type: "commentaries",
			slug: "homer",
			_id: "oq84gbl",
			itemsCount: "5,276 comments",
			files: [{
				name: "hector.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'English',
			type: 'commentary',
			tags: ['Homer', 'Epics', 'In Progress'],
			authors: ['Gregory Nagy', 'Leonard Muellner',"Anita Nikkanen", "Casey Dué", "Corinne Pache", "David Elmer", "Douglas Frame", "Mary Ebbott", "Olga Levaniouk", "Richard Martin", "Thomas Walsh", "Yiannis Petropoulos"],

		}, {
			title: "A Pindar Commentary in Progress",
			textShort: "Gregory Nagy and Maša Culumovic",
			description: "An evolving, collaborative commentary based on the cumulative research of Milman Parry and Albert Lord, who created a new way of thinking about Homeric poetry. Based on the research of Milman Parry and Albert Lord, the Homeric poems are the product of a performance tradition that stretches back to the period of Indo-European community, the 3rd Millennium BCE. Breaking from conventional Homer research, our approach to Homeric diction is profoundly anthropological and, if we can help it, never judgmental",
			type: "commentaries",
			slug: "pindar",
			_id: "4t98ogu",
			itemsCount: "1,489 comments",
			files: [{
				name: "ajax_achilles_3.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'English',
			type: 'commentary',
			tags: ['Pindar', 'In Progress'],
			authors: ['Gregory Nagy', 'Maša Culumovic']

		}, {
			title: "A Pausanias Commentary in Progress",
			textShort: "Gregory Nagy, Carolyn Higbie, Greta Hawes, and 2 others",
			description: "An evolving, collaborative commentary based on the cumulative research of Milman Parry and Albert Lord, who created a new way of thinking about Homeric poetry. Based on the research of Milman Parry and Albert Lord, the Homeric poems are the product of a performance tradition that stretches back to the period of Indo-European community, the 3rd Millennium BCE. Breaking from conventional Homer research, our approach to Homeric diction is profoundly anthropological and, if we can help it, never judgmental",
			type: "commentaries",
			slug: "pausanias",
			_id: "38of00m",
			itemsCount: "255 comments",
			files: [{
				name: "iris.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'English',
			type: 'commentary',
			tags: ['Pausanias', 'In Progress'],
			authors: ['Gregory Nagy', 'Carolyn Higbie', 'Greta Hawes']

		}, {
			title: "A Herodotus Commentary in Progress",
			textShort: "Gregory Nagy, Leonard Muellner, and Doug Frame",
			description: "An evolving, collaborative commentary based on the cumulative research of Milman Parry and Albert Lord, who created a new way of thinking about Homeric poetry. Based on the research of Milman Parry and Albert Lord, the Homeric poems are the product of a performance tradition that stretches back to the period of Indo-European community, the 3rd Millennium BCE. Breaking from conventional Homer research, our approach to Homeric diction is profoundly anthropological and, if we can help it, never judgmental",
			type: "commentaries",
			slug: "herodotus",
			_id: "31di93a",
			itemsCount: "312 comments",
			files: [{
				name: "sirens.jpg",
				type: "image/jpeg",
			}],
			originalLanguage: 'English',
			type: 'commentary',
			tags: ['Pausanias', 'In Progress'],
			authors: ['Gregory Nagy', 'Leonard Muellner', 'Doug Frame']
		}];

		let filterLookup = {};
		filterLookup.authors =  {
			name: 'Authors',
			type: 'text',
			values: []
		}
		filterLookup.originalLanguage =  {
			name: 'Original Language',
			type: 'text',
			values: []
		}
		filterLookup.tags =  {
			name: 'Tags',
			type: 'text',
			values: []
		}

		let filteredItems = []
		commentaries.forEach( (commentary) => {
			let matchesFilters = true;
			if (!filterLookup.originalLanguage.values.includes(commentary.originalLanguage)) {
				filterLookup.originalLanguage.values.push(commentary.originalLanguage)
			}
			if (commentary.authors) {
				commentary.authors.forEach((author) => {
					if (!filterLookup.authors.values.includes(author)) {
						filterLookup.authors.values.push(author)
					}
				})
			}
			if (commentary.tags) {
				commentary.tags.forEach((tag) => {
					if (!filterLookup.tags.values.includes(tag)) {
						filterLookup.tags.values.push(tag)
					}
				})
			}

			if (query) {
				if (query['Original Language'] && commentary.originalLanguage !== query['Original Language']) {
					matchesFilters = false;
				}
				if (query['Authors'] ) {
					matchesFilters = false;
					commentary.authors.forEach((author) => {
						if (query['Authors'].split('+').includes(author)) {
							matchesFilters = true;
						}
					})
				}
				if (query['Tags'] ) {
					matchesFilters = false;
					commentary.tags.forEach((tag) => {
						if (query['Tags'].split('+').includes(tag)) {
							matchesFilters = true;
						}
					})
				}
			}
			if (matchesFilters) {
				filteredItems.push(commentary)
			}
		})

		let relevantFiltersLookup = {};
		relevantFiltersLookup.authors =  {
			name: 'Authors',
			type: 'text',
			values: []
		}
		relevantFiltersLookup.originalLanguage =  {
			name: 'Original Language',
			type: 'text',
			values: []
		}
		relevantFiltersLookup.tags =  {
			name: 'Tags',
			type: 'text',
			values: []
		}
		filteredItems.forEach( (commentary) => {
			if (!relevantFiltersLookup.originalLanguage.values.includes(commentary.originalLanguage)) {
				relevantFiltersLookup.originalLanguage.values.push(commentary.originalLanguage)
			}
			if (commentary.authors) {
				commentary.authors.forEach((author) => {
					if (!relevantFiltersLookup.authors.values.includes(author)) {
						relevantFiltersLookup.authors.values.push(author)
					}
				})
			}
			if (commentary.tags) {
				commentary.tags.forEach((tag) => {
					if (!relevantFiltersLookup.tags.values.includes(tag)) {
						relevantFiltersLookup.tags.values.push(tag)
					}
				})
			}


		})


		const filters = Object.keys(filterLookup).map((filter_name) => {
			return filterLookup[filter_name]
		})
		const relevantFilters = Object.keys(relevantFiltersLookup).map((filter_name) => {
			return relevantFiltersLookup[filter_name]
		})
		return (
			<div className={_classes.join(' ')}>
				<h5>Commentaries /</h5>
				<h2>All Commentaries</h2>
				<div className="facetedCardsContent">
					<Sticky
						className="facetedCardsContentFilters"
						activeClass="-sticky"
						bottomBoundary=".sticky-reactnode-boundary"
						enabled
					>
						<div className="facetedCardsContentFiltersInner">
							{false && this.props.loading ? (
								<CommentariesFiltersContainer loading />
							) : (
								<CommentariesFiltersContainer filters={filters} relevantFilters={relevantFilters} />
							)}
						</div>
					</Sticky>
					<div className="facetedCardsContentCards">
						<List
							loading={this.props.loading}
							selectable={this.props.selectable}
							items={commentaries}
							isAdmin={this.props.isAdmin}
						/>
					</div>
				</div>
			</div>
		);
	}
}

CommentariesFacetedCards.propTypes = {
	theme: PropTypes.string,
	selectable: PropTypes.bool,
	isAdmin: PropTypes.bool,

}

CommentariesFacetedCards.defaultProps = {
	theme: '',
};


export default CommentariesFacetedCards;
