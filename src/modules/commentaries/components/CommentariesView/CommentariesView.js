import React from 'react';

import Root from '../../../../containers/Root';
import CommentariesFacetedCards from '../../components/CommentariesFacetedCards';


const CommentariesView = () => (
	<Root>
		<CommentariesFacetedCards />
	</Root>
);

export default CommentariesView;
