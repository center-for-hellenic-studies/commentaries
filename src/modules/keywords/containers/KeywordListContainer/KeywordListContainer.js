import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'react-apollo';

// component
import KeywordList from '../../components/KeywordList';

import keywords from '../../lib/keywords';


const KeywordListContainer = props => {
	let _keywords = keywords;

	// do preprocessing if necessary
	if (props.type) {
		_keywords = keywords.filter((keyword) => (
			keyword.type === props.type
		));
	}

	if (props.limit) {
		_keywords = _keywords.slice(0, props.limit);
	}

	return (
		<KeywordList
			{...props}
			keywords={_keywords}
		/>
	);
};

export default KeywordListContainer;
