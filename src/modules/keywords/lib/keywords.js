const keywords = [
	{
		"_id": "xNyLkcYiKzKvEqtpn",
		"title": "\"Aeolic default\"",
		"slug": "aeolic-default",
		"description": null,
		"descriptionRaw": null,
		"type": "idea",
		"count": 0,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "3FtAaKguMzRWuSuhf",
		"title": "\"Cretan lies\"",
		"slug": "\"cretan-lies\"",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "2HCW4DfZuQfpweSan",
		"title": "\"plus verse\"",
		"slug": "plus-verse",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "opS4WLbwgg8smdLrF",
		"title": "\"speaking name\" (nomen loquens)",
		"slug": "speaking-name-nomen-loquens",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "rjJKssszx9YQEChnP",
		"title": "\"taking the hit\"",
		"slug": "taking-the-hit",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "MS4i9wmJJdKjm94T7",
		"title": "'best of the Achaeans'",
		"slug": "best-of-the-achaeans",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "7eG7NqmdbnSag77mN",
		"title": "'delight' / 'take delight'",
		"slug": "delight-take-delight",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 0,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "rH3Z6b3bjYjHm7tJG",
		"title": "'the two Ajaxes'",
		"slug": "the-two-ajaxes",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "g43Tk6uQ927P7evQB",
		"title": "(apo-)phthinesthai ‘wilt; perish’",
		"slug": "apo-phthinesthai-wilt-perish",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "yAt7tfjcL2xQDi824",
		"title": "A Sampling of Comments on Pausanias",
		"slug": "a-sampling-of-comments-on-pausanias",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "CupW6dfGEFR5jYHnn",
		"title": "A Sampling of Comments on Pindar",
		"slug": "a-sampling-of-comments-on-pindar",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "b37M7ZyY8ZLvThErD",
		"title": "A Sampling of Comments on the Iliad",
		"slug": "a-sampling-of-comments-on-the-iliad",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "7KigvcWFh97ENpaM4",
		"title": "A Sampling of Comments on the Odyssey",
		"slug": "a-sampling-of-comments-on-the-odyssey",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "W4qTYxygoP9BProMS",
		"title": "Abai",
		"slug": "abai",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "miBwNzSsKL5dnv7Sp",
		"title": "Abas son of Lynkeus",
		"slug": "abas-son-of-lynkeus",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "cMcKv7HX9BCpYpfpG",
		"title": "Abia",
		"slug": "abia",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "ZuyqeYgWQx9JCHsRx",
		"title": "Achaean Wall",
		"slug": "achaean-wall",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "T3j5seogfMnFSB8ne",
		"title": "Achaeans=Danaans=Argives",
		"slug": "achaeansdanaansargives",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "yZjiY98CNQAFFuhTx",
		"title": "Achelous",
		"slug": "achelous",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "AzSzDotFJF8R9AFLJ",
		"title": "Achilles",
		"slug": "achilles-1",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "jzzTktz23sfsL7iD5",
		"title": "Achilles",
		"slug": "achilles",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "kh3yD8hBdfouX5QsM",
		"title": "Achilles as a prefigured corpse",
		"slug": "achilles-as-a-prefigured-corpse",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "yDLCHJnnqyyHaAwxQ",
		"title": "Achilles as lover",
		"slug": "achilles-as-lover",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "P5AK2HKqn46dXbwYu",
		"title": "Achilles conquers 11 cities on foot and 12 cities by way of ships",
		"slug": "achilles-conquers-11-cities-on-foot-and-12-cities-by-way-of-ships",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "6cB4odT9MYT7whXcN",
		"title": "Achilles cuts his hair",
		"slug": "achilles-cuts-his-hair",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "kDjgxjum95TR4PamQ",
		"title": "Achilles son of Peleus",
		"slug": "achilles-son-of-peleus",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "4K234rxNqFpoe5yLz",
		"title": "Achilles the Aeolian",
		"slug": "achilles-the-aeolian",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "NhcoyfFsK7EhQF7AP",
		"title": "Admetos son of Augeios",
		"slug": "admetos-son-of-augeios",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "p3qNnqTcLkGZBrceX",
		"title": "Adonis",
		"slug": "adonis",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "EyG5Gn7Crz46SByHn",
		"title": "Adrastos",
		"slug": "adrastos",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "2tMTBLiYJpZWDaAfz",
		"title": "Adrastos son of Talaus",
		"slug": "adrastos-son-of-talaus",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "xR8fARXRFSoB7v4Sg",
		"title": "Adunaton oracle story",
		"slug": "adunaton-oracle-story",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "6EazEo33fJNRyqMmq",
		"title": "Aegean Sea",
		"slug": "aegean-sea",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "mXzj59G8EDSoe6WPP",
		"title": "Aegeus",
		"slug": "aegeus",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "iD2w8gM3ihx9CgSuS",
		"title": "Aegina",
		"slug": "aegina",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "RGRBu35g88FZoRCbK",
		"title": "Aegina and Thebe linked as nymphs",
		"slug": "aegina-and-thebe-linked-as-nymphs",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "WyP4DYjF7hjyFDmhf",
		"title": "Aegina and Thebes linked as states",
		"slug": "aegina-and-thebes-linked-as-states",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "aMPL8Fi7fCpcD7WJi",
		"title": "Aegina the nymph",
		"slug": "aegina-the-nymph",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "8oxNpQF5brRDotBuM",
		"title": "Aeginetans",
		"slug": "aeginetans",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "BAydZTPH3omXiB3Zf",
		"title": "Aeneas son of Anchises and",
		"slug": "aeneas-son-of-anchises-and",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "95BMZFLeWR9uxiKXr",
		"title": "Aeneas son of Anchises and Aphrodite",
		"slug": "aeneas-son-of-anchises-and-aphrodite",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "DfEn3zpoQY4LFLhbN",
		"title": "Aeneas the Aeolian",
		"slug": "aeneas-the-aeolian",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "bYpncJjBevuQ8sX7r",
		"title": "Aeneas the Ionian",
		"slug": "aeneas-the-ionian",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "Huz8MoezdFRv4HGvc",
		"title": "Aeneid",
		"slug": "aeneid",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "P59Yq74c5oC9PT9oh",
		"title": "Aeolian",
		"slug": "aeolian",
		"description": "<p>As a noun, this word refers to Greek-speaking people who spoke an ancient Greek dialect known as Aeolic. As an adjective, this same word refers to the social and cultural institutions of these Aeolic-speaking people. </p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "TA4q4NJstmhagGbHi",
		"title": "Aeolian",
		"slug": "aeolian",
		"description": null,
		"descriptionRaw": null,
		"type": "idea",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "qFbHiweEhXPe4Anb8",
		"title": "Aeolian",
		"slug": "aeolian",
		"description": "<p>As a noun, this word refers to Greek-speaking people who spoke an ancient Greek dialect known as Aeolic. As an adjective, this same word refers to the social and cultural institutions of these Aeolic-speaking people. [[GN 2016.09.07.]]</p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "f7KBkzxcCGBamxT8r",
		"title": "Aeolian Dodecapolis",
		"slug": "aeolian-dodecapolis",
		"description": "<p>A confederation of twelve Aeolian cities located on the mainland of Asia Minor. Herodotus 1.149.1 lists them in the following order: Cyme, Lērisai, Neon Teikhos, Tēmnos, Killa, Notion, Aigiroessa, Pitanē, Aigaiai, Myrina, Gryneia, and, lastly, Smyrna. Herodotus 1.151.1 notes that the Aeolian cities on the mainland of Asia Minor in the region of Mount Ida were grouped separately from the Aeolian Dodecapolis, and he does not list those cities by name. Then there is Lesbos: Herodotus 1.151.2 says that this island was politically organized as a federation of five Aeolian cities. (Further details in HPC 138.) In one of the <em>Lives of Homer</em>, Vita 1.18–19, the Aeolian city of Smyrna is described as the daughter city of Cyme; also, in Vita 1.17–31, Smyrna is recognized as the city where Homer was born. The same point is made by Strabo 14.1.37 C646, who recognizes the special claim of Smyrna as the birthplace of Homer while duly noting the counterclaims of rival cities. (Further details in HPC 135.) The Aeolian Dodecapolis became destabilized when Smyrna was captured by the Ionians. The story of the capture is told by Herodotus 1.149–150, who adds at 1.150.2 this detail: the stranded Aeolians of Smyrna were then absorbed by the remaining eleven Aeolian cities of the original Dodecapolis. (Further details in HPC 138–139.) With the capture of Smyrna by the Ionians, not only was the Aeolian Dodecapolis destabilized: even the identity of Homer as an Aeolian native of Smyrna was reconfigured. We can observe the reconfiguration by tracking Homer’s shifting identity in two <em>Lives of Homer</em>. In one of these <em>Lives</em>, Vita 1.3–17 and 1.17.31, Homer is conceived in Aeolian Cyme and born in Aeolian Smyrna. In terms of this <em>Life</em>, the time of Homer’s conception and birth is situated in a mythical era that precedes the Ionian Migration. This version stands in sharp contrast to the version we read in another <em>Life</em>, Vita 2.9–12 (and in other sources as well): in this version, as in the previous version, Homer is born in Smyrna, but now his birth is synchronized with the Ionian Migration, which happens admittedly later than the Aeolian Migration. (See under <em>Aeolian Migration</em> and <em>Ionian Migration</em>.) So, the Homer who had originated from a diminished Aeolian Dodecapolis now becomes reconfigured as a Homer who originates from an augmented Ionian Dodecapolis. (Further details in HPC 134, 142, 211.) </p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "Nc3PRGPChaC2tY3TX",
		"title": "Aeolian Migration",
		"slug": "aeolian-migration",
		"description": "<p>The Greek word translated here as ‘migration’ is <em>apoikiā</em>; a closer translation would be ‘colonization’, with reference to myths about settlers who settled the Aeolian coastlands of Asia Minor and the major offshore Aeolian islands of Lesbos and Tenedos. According to Strabo 13.1.3 C 582, the <em>Aiolikē apoikiā</em> ‘Aeolic colonization’ started sixty years after the Trojan war and four generations before the start of the <em>Iōnikē apoikia</em> ‘Ionian colonization’. </p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "hKS5CKvthgwmvrfBF",
		"title": "Aeolian poetics",
		"slug": "aeolian-poetics",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "3xALKsoWtjjd44RPQ",
		"title": "Aeolian traditions about the Trojan War",
		"slug": "aeolian-traditions-about-the-trojan-war",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "cHqeddDPrb9csWS3X",
		"title": "Aeolian women in the Iliad",
		"slug": "aeolian-women-in-the-iliad",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "thpfi8C5r6tvm3Eyu",
		"title": "Aeolian, Aeolic",
		"slug": "aeolian-aeolic-1",
		"description": "<p>As a noun, Aeolian word refers to Greek-speaking people who spoke an ancient Greek dialect known as Aeolic. As an adjective, this same word refers to the social and cultural institutions of these Aeolic-speaking people. Aeolic is a major dialectal branch of the ancient Greek language. It is the “recessive” dialect of Homeric diction, as opposed to Ionic, which is the “dominant” dialect. See also under <em>Ionic</em>; also under <em>Homeric diction</em>. </p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "oh3PHtfwavjhk57i2",
		"title": "Aeolians",
		"slug": "aeolians",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "23pdY2ryAj3tPuTJm",
		"title": "Aeolic",
		"slug": "aeolic",
		"description": "<p>A major dialectal branch of the ancient Greek language. It is the “recessive” dialect of Pindar’s poetic diction, as opposed to Doric, which is the “dominant” dialect. See also under Ionic. [[GN 2017.09.27.]]</p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "38m35uPrzHQK2WREg",
		"title": "Aeolic",
		"slug": "aeolic",
		"description": null,
		"descriptionRaw": null,
		"type": "idea",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "wMgXzX3GhTXC4AjaW",
		"title": "Aeolic",
		"slug": "aeolic",
		"description": "<p>A major dialectal branch of the ancient Greek language. It is the “recessive” dialect of Pindar’s poetic diction, as opposed to Doric, which is the “dominant” dialect. See also under <em>Ionic</em>. </p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "88uTToTXnXeoicEox",
		"title": "Aeolic default",
		"slug": "aeolic-default",
		"description": null,
		"descriptionRaw": null,
		"type": "idea",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "F4E8axMcudQ9CBdZY",
		"title": "Aeolic default",
		"slug": "aeolic-default",
		"description": "<p>Homeric diction defaults to Aeolic forms in the absence of corresponding Ionic forms. (Details in Nagy 2011b:175.) See under Aeolic; also under Ionic; also under Homeric diction. </p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "RgCxpyjpgaqzY5ZoT",
		"title": "Aesop as exponent of praise/blame",
		"slug": "aesop-as-exponent-of-praise-blame",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 0,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "FETa5tnff8RQzqLhr",
		"title": "Aesop as pharmakos ‘scapegoat’",
		"slug": "aesop-as-pharmakos-scapegoat",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 0,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "xtwJqxhN8YaL33HNZ",
		"title": "Aetiology",
		"slug": "aetiology",
		"description": "<p>A myth that explicitly motivates (1) a ritual or (2) a custom that includes ritualized behavior. It cannot be assumed that such a myth is independent of the ritual that it motivates: rather, the myth can be considered to be a part of the ritual that is ostensibly being motivated by the myth. </p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "vHHebpAcakLJuJ6uJ",
		"title": "Agamedes",
		"slug": "agamedes",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "y3C46mkXCnSWzcGWm",
		"title": "Agamemnon",
		"slug": "agamemnon",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "NZz2RjMSJNs6MvK6M",
		"title": "Agamemnon declares that he is making an offer",
		"slug": "agamemnon-declares-that-he-is-making-an-offer",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "LnxKpDKoRPPt3cK9s",
		"title": "Agamemnon son of Atreus",
		"slug": "agamemnon-son-of-atreus",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "tYQv39oEm85zTdri3",
		"title": "Agamemnon’s offer of compensation",
		"slug": "agamemnon's-offer-of-compensation",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "aap292hP28M7mmvp2",
		"title": "Agenor",
		"slug": "agenor",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "ebPFRAbPtcXgEqSxz",
		"title": "Agenor son of Triopas",
		"slug": "agenor-son-of-triopas",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "rqv6btS4xpM5XbYDv",
		"title": "Aglauros",
		"slug": "aglauros",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "ZHEazCgKbwhmw4LxK",
		"title": "Aglauros, Hersē, Pandrosos",
		"slug": "aglauros-hers-pandrosos",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "J62JWe9p9rQjebXBz",
		"title": "Aiaia",
		"slug": "aiaia",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "word",
		"count": 0,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "Svav8pGsXqCLjHgWa",
		"title": "Aiakidai",
		"slug": "aiakidai",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "W9m2eRLsbZFmC6ZNE",
		"title": "Aiakos",
		"slug": "aiakos",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "Khw5RQNcYMa5xzGqy",
		"title": "Aiante",
		"slug": "aiante",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "zHJyzirRpH2quzi9B",
		"title": "Aiante; with or without Teukros",
		"slug": "aiante-with-or-without-teukros",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "DQcT82DkC6a4dwaPW",
		"title": "Aias",
		"slug": "aias",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "RjtLJi44nodnCuP8Z",
		"title": "Aigaion",
		"slug": "aigaion",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 0,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "BDHDnJ5WSyEvoWW77",
		"title": "Aigaleus son of Adrastos",
		"slug": "aigaleus-son-of-adrastos",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "pvsDMxWNHYSYALH7S",
		"title": "Aigeus",
		"slug": "aigeus",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "GdnPhjRD6pnWsnyPr",
		"title": "Aigialeos",
		"slug": "aigialeos",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "mZcAoQ7QnguHqauf8",
		"title": "Aigialeus son of Adrastos",
		"slug": "aigialeus-son-of-adrastos",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "HvuRPNLawq7PM739C",
		"title": "Aigialus son of Adrastos",
		"slug": "aigialus-son-of-adrastos",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "7cK9HJqjKikjBSfXP",
		"title": "Aigistheus",
		"slug": "aigistheus",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "zSjmiPLnQn8RTkvsK",
		"title": "Aigisthos",
		"slug": "aigisthos",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "KZvwyxiJFEsewpusK",
		"title": "Aipytos son of Herakles",
		"slug": "aipytos-son-of-herakles",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "jSiXFazg5SkdgfMPR",
		"title": "Aipytos son of Kresphontes",
		"slug": "aipytos-son-of-kresphontes",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "FFdM4ahJchsJuhcSD",
		"title": "Aithiopes 'Aethiopians'",
		"slug": "aithiopes-aethiopians",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "HLjKMzrFkwCWxnEjh",
		"title": "Aithiopis",
		"slug": "aithiopis",
		"description": "<p>See under <em>epic Cycle</em>. </p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "KL56dNyisBZSB4ba6",
		"title": "Aithiopis",
		"slug": "aithiopis",
		"description": "<p>See under <em>epic Cycle</em>.</p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "fPLy9uGQBmfTWWjM3",
		"title": "Aithiopis",
		"slug": "aithiopis-2",
		"description": "<p>See under <em>epic Cycle</em>.</p>",
		"descriptionRaw": null,
		"type": "idea",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "7LKo7LQMBxyDSArmc",
		"title": "Aithouse daughter of Poseidon",
		"slug": "aithouse-daughter-of-poseidon",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "YNaWdDdfZb8P9NJTX",
		"title": "Aithra mother of Theseos",
		"slug": "aithra-mother-of-theseos",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "MdJMHwwQKjtA2doA2",
		"title": "Aithōn",
		"slug": "aith-n",
		"description": "<p></p>",
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "oH25k7bwqbZXQEJZH",
		"title": "Aitnaios son of Prometheus",
		"slug": "aitnaios-son-of-prometheus",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "n9MtoyJh6hFuyJcmc",
		"title": "Ajax",
		"slug": "ajax",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "Guddgyg4Nr7ak4XHc",
		"title": "Ajax (Aias)",
		"slug": "ajax-aias",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "km6CCNmQF7WEg7nce"
	},
	{
		"_id": "GpxtC6zgZugD6XiJD",
		"title": "Ajax and Teukros",
		"slug": "ajax-and-teukros",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": null,
		"tenantId": "4tJPty4gis3AhGt6z"
	},
	{
		"_id": "tuc7EapePBgMA7ScA",
		"title": "Akamas son of Theseus",
		"slug": "akamas-son-of-theseus",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 1,
		"tenantId": "5Ns98kBwQAvi6sEyK"
	},
	{
		"_id": "Ljwxa3MiXtZdzwo7g",
		"title": "Akhaio-/Akhaiā-",
		"slug": "akhaio-akhai",
		"description": null,
		"descriptionRaw": null,
		"type": "word",
		"count": 0,
		"tenantId": "4tJPty4gis3AhGt6z"
	}
];

export default keywords;
