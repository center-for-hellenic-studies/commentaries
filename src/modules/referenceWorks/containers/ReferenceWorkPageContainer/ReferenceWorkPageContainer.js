import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'react-apollo';

// components
import ReferenceWorkPage from '../../components/ReferenceWorkPage';

// graphql
import projectsQuery from '../../../settings/graphql/queries/list';


const ReferenceWorkPageContainer = ({ projectsQuery, tenantId }) => {
	let settings = {};

	if (
		projectsQuery
    && projectsQuery.project
	) {
  	settings = projectsQuery.project.find(setting => setting.tenantId === tenantId)
	}


	return (
		<ReferenceWorkPage
			settings={settings}
		/>
	);
};


const mapStateToProps = (state, props) => ({
	tenantId: state.tenant.tenantId,
});


export default compose(
	connect(mapStateToProps),
	projectsQuery,
)(ReferenceWorkPageContainer);
