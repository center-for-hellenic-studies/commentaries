/**
 * @prettier
 */

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import ReadingContainer from '../containers/ReadingContainer';

const ReadingRoutes = ({ match }) => (
	<Switch>
		<Route path={`${match.url}/:urn`} component={ReadingContainer} />
	</Switch>
);

export default <Route path="/read" component={ReadingRoutes} />;
