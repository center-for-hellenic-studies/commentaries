/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';

import App from '../components/App.js';

const ReadingContainer = ({ match = {} }) => {
	const urn = (match.params || {}).urn;

	return <App urn={urn} />;
};

ReadingContainer.propTypes = {
	match: PropTypes.shape({
		params: PropTypes.shape({
			urn: PropTypes.string.isRequired,
		}),
	}),
};

export default ReadingContainer;
