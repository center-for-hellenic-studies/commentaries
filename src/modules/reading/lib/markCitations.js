/**
 * Create revision markup with cross references in the commentary
 * @param {string} html - an html input string
 * @returns {Object} an object with the processed html output string
 */
const markCitations = (html) => {
	let newHtml = html;

	const workNamesSpace = [{
		title: 'Iliad',
		slug: 'iliad',
		tlgCitation: 'tlg0012.tlg001.perseus-grc2',
	}, {
		title: 'Odyssey',
		slug: 'odyssey',
		tlgCitation: 'tlg0012.tlg002.perseus-grc2',
	}, {
		title: 'Homeric Hymns',
		slug: 'hymns',
		tlgCitation: 'tlg0013.tlg001.perseus-grc2',
	}, {
		title: 'Hymns',
		slug: 'hymns',
		tlgCitation: 'tlg0013.tlg001.perseus-grc2',
	}];
	const workNamesPeriod = [{
		title: 'Il',
		slug: 'iliad',
		tlgCitation: 'tlg0012.tlg001.perseus-grc2',
	}, {
		title: 'Od',
		slug: 'odyssey',
		tlgCitation: 'tlg0012.tlg002.perseus-grc2',
	}, {
		title: 'HH',
		slug: 'hymns',
		tlgCitation: 'tlg0013.tlg001.perseus-grc2',
	}, {
		title: 'I',
		slug: 'iliad',
		tlgCitation: 'tlg0012.tlg001.perseus-grc2',
	}, {
		title: 'O',
		slug: 'odyssey',
		tlgCitation: 'tlg0012.tlg002.perseus-grc2',
	}];

	let regex1;
	let regex2;

	workNamesSpace.forEach((workName) => {
		// regex for range with dash (lookahead to ignore if surrounded by &quot; - required for comment cross reference)
		regex1 = new RegExp(`${workName.title} (\\d+).(\\d+)[\\-\\–\\—](\\d+)(?!.*&quot;)`, 'g');

		// regex for no range (and lookahead to ensure range isn't captured) (lookahead to ignore if surrounded by &quot; - required for comment cross reference)
		regex2 = new RegExp(`${workName.title} (\\d+).(?!\\d+[\\-\\–\\—]\\d+)(\\d+)(?!.*&quot;)`, 'g');

		newHtml = newHtml.replace(regex1,
			`<a
        href="/read/urn:cts:greekLit:${workName.tlgCitation}:$1.$2"
				class='has-lemma-reference'
				data-work=${workName.slug}
				data-subwork='$1'
				data-lineFrom='$2'
				data-lineTo='$3'
			>${workName.title} $1.$2-$3</a>`);
		newHtml = newHtml.replace(regex2,
			`<a
        href="/read/urn:cts:greekLit:${workName.tlgCitation}:$1.$2"
				class='has-lemma-reference'
				data-work=${workName.slug}
				data-subwork='$1'
				data-lineFrom='$2'
			>${workName.title} $1.$2</a>`);
	});

	workNamesPeriod.forEach((workName) => {
		// regex for range with dash (lookahead to ignore if surrounded by &quot; - required for comment cross reference)
		regex1 = new RegExp(`([^\\w+])${workName.title}.(\\s*)(\\d+).(\\d+)[\\-\\–\\—](\\d+)(?!.*&quot;)`, 'g');

		// regex for no range (and lookahead to ensure range isn't captured) (lookahead to ignore if surrounded by &quot; - required for comment cross reference)
		regex2 = new RegExp(`([^\\w+])${workName.title}.(\\s*)(\\d+).(?!\\d+[\\-\\–\\—]\\d+)(\\d+)(?!.*&quot;)`, 'g');
		newHtml = newHtml.replace(regex1,
			`$1<a
        href="/read/urn:cts:greekLit:${workName.tlgCitation}:$3.$4"
				class='has-lemma-reference'
				data-work=${workName.slug}
				data-subwork='$3'
				data-lineFrom='$4'
				data-lineTo='$5'
			>${workName.title}.$2$3.$4-$5</a>`);
		newHtml = newHtml.replace(regex2,
			`$1<a
        href="/read/urn:cts:greekLit:${workName.tlgCitation}:$3.$4"
				class='has-lemma-reference'
				data-work=${workName.slug}
				data-subwork='$3'
				data-lineFrom='$4'
			>${workName.title}.$2$3.$4</a>`);
	});

	return newHtml;
};

export default markCitations;
