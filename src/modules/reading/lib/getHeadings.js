/**
 * @prettier
 */

import React from 'react';

const handleClick = (cb, urn) => e => {
	e.preventDefault();

	cb(urn, `Commenting on ${urn}:`);
};

const getHeadings = (
	location,
	prevLocation,
	refsDecls,
	urn = '',
	cb = () => {}
) => {
	let heading, subheading, subsubheading;

	if (location.length > 1) {
		// Only compare up to the second-most granular location,
		// e.g., if we have [book, chapter, line], only
		// check if "book" (h1) and "chapter" (h2) have changed.

		heading =
			location[0] !== prevLocation[0] ? (
				<h1
					className="serif"
					onClick={handleClick(cb, `${urn}.${location[0]}`)}
					style={{ textTransform: 'capitalize' }}
				>
					{refsDecls[0].label} {location[0]}
				</h1>
			) : null;

		if (location.length > 2) {
			subheading =
				location[1] !== prevLocation[1] ? (
					<h2
						className="serif"
						onClick={handleClick(cb, `${urn}.${location[0]}.${location[1]}`)}
						style={{ textTransform: 'capitalize' }}
					>
						{location[0]}.{location[1]}
					</h2>
				) : null;

			if (location.length > 3) {
				subsubheading =
					location[2] !== prevLocation[2] ? (
						<h3
							className="serif"
							onClick={handleClick(
								cb,
								`${urn}.${location[0]}.${location[1]}.${location[2]}`
							)}
							style={{ textTransform: 'capitalize' }}
						>
							{location[0]}.{location[1]}.{location[2]}
						</h3>
					) : null;
			}
		}
	}

	return { heading, subheading, subsubheading };
};

export default getHeadings;
