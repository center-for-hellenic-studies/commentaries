/**
 * @prettier
 */

import gql from 'graphql-tag';

const createCommentMutation = gql`
	mutation commentCreate($hostname: String, $comment: CommentInputType!) {
		commentCreate(comment: $comment, hostname: $hostname) {
			_id
		}
	}
`;

export default createCommentMutation;
