/**
 * @prettier
 */

import gql from 'graphql-tag';

const textGroupQuery = gql`
	query textGroupQuery($id: Int) {
		TEXT_textGroup(id: $id) {
			id
			title
			works {
				description
				english_title
				form
				full_urn
				id
				label
				original_title
				slug
				urn
				work_type
				language {
					id
					title
					slug
				}
				version {
					description
					id
					slug
					title
					urn
				}
				exemplar {
					description
					id
					slug
					title
					urn
				}
				translation {
					description
					id
					slug
					title
					urn
				}
			}
		}
	}
`;

export default textGroupQuery;
