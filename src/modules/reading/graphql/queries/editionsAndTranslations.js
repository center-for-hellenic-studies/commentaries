/**
 * @prettier
 */

import gql from 'graphql-tag';

const query = gql`
	query editionsAndTranslationsQuery($urn: TEXT_CtsUrn) {
		TEXT_works(urn: $urn) {
			id
			english_title
			original_title
			structure
			form
			language {
				id
				title
			}
			exemplar {
				id
				title
				description
				urn
			}
			version {
				id
				title
				description
				urn
			}
			translation {
				id
				title
				description
				urn
			}
			refsDecls {
				description
				label
				match_pattern
				replacement_pattern
				slug
			}
		}
	}
`;

export default query;
