/**
 * @prettier
 */

import gql from 'graphql-tag';

const query = gql`
	query textGroupsQuery(
		$textSearch: String
		$urn: TEXT_CtsUrn
		$limit: Int
		$offset: Int
	) {
		TEXT_textGroups(
			textsearch: $textSearch
			urn: $urn
			limit: $limit
			offset: $offset
		) {
			id
			slug
			title
			urn
			works {
				language {
					id
					title
					slug
				}
			}
		}
	}
`;

export default query;
