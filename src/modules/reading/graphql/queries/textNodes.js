/**
 * @prettier
 */

import gql from 'graphql-tag';

// $urn can include citations and subrefs
const query = gql`
	query textNodesQuery(
		$urn: String!
		$startsAtLocation: [Int]
		$endsAtLocation: [Int]
	) {
		TEXT_workByUrn(full_urn: $urn) {
			id
			textNodes(
				startsAtLocation: $startsAtLocation
				endsAtLocation: $endsAtLocation
			) {
				id
				index
				location
				text
				urn
			}
		}
	}
`;

export default query;
