/**
 * @prettier
 */

import gql from 'graphql-tag';

// $urn can include citations and subrefs
const query = gql`
	query textWorksQuery(
		$urn: TEXT_CtsUrn!
		$endIndex: Int
		$startIndex: Int
		$offset: Int
	) {
		TEXT_works(urn: $urn) {
			id
			textLocationNext(index: $endIndex, offset: $offset)
			textLocationPrev(index: $startIndex, offset: $offset)
		}
	}
`;

export default query;
