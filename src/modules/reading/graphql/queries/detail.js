/**
 * @prettier
 */

import gql from 'graphql-tag';

const query = gql`
	query textDetailQuery($full_urn: String, $short_urn: TEXT_CtsUrn) {
		TEXT_works(full_urn: $full_urn) {
			id
			urn
			full_urn
			slug
			english_title
			original_title
			form
			work_type
			label
			description
			language {
				id
				title
				slug
			}
			version {
				id
				slug
				title
				urn
				description
			}
			exemplar {
				id
				slug
				title
				description
			}
			translation {
				id
				slug
				title
				description
			}
			refsDecls {
				description
				label
			}
		}
		TEXT_textGroups(urn: $short_urn) {
			id
			title
		}
	}
`;

export default query;
