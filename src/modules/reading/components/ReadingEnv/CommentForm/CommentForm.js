/**
 * @prettier
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import FlatButton from 'material-ui/FlatButton';
import { EditorState, convertFromRaw, convertToRaw } from 'draft-js';
import { useMutation } from '@apollo/react-hooks';

import OrpheusEditor from '../../../../orpheus-editor/components/Editor';

import createCommentMutation from '../../../graphql/mutations/createCommentMutation';
import updateCommentMutation from '../../../graphql/mutations/updateCommentMutation';
import decorators from '../../../../orpheus-editor/components/decorators';

const CommentForm = ({
	comment = {},
	commentTarget,
	hideCommentForm,
	projectId,
	toggleSidePanel,
	urn,
}) => {
	// https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html#recommendation-fully-uncontrolled-component-with-a-key
	const revision = comment.latestRevision || {};
	const [localContent, setLocalContent] = useState(
		Boolean(revision.text)
			? EditorState.createWithContent(
				convertFromRaw(JSON.parse(revision.text)),
				decorators
			  )
			: EditorState.createEmpty()
	);
	const [localTitle, setLocalTitle] = useState(revision.title || '');
	const [submitting, setSubmitting] = useState(false);
	const [
		_createComment,
		{
			data: createCommentData,
			error: createCommentError,
			loading: createCommentLoading,
		},
	] = useMutation(createCommentMutation, {
		refetchQueries: ['projectCommentsQuery'],
	});
	const [
		_updateComment,
		{
			data: updateCommentData,
			error: updateCommentError,
			loading: updateCommentLoading,
		},
	] = useMutation(updateCommentMutation, {
		refetchQueries: ['projectCommentsQuery'],
	});
	useEffect(() => {
		if (createCommentData || updateCommentData) {
			hideCommentForm();
		}

		if (createCommentError || updateCommentError) {
			setSubmitting(false);
		}

		// keep the panel open if the user is updating
		// a comment
		if (createCommentData) {
			toggleSidePanel();
		}
	}, [
		createCommentData,
		updateCommentData,
		createCommentError,
		updateCommentError,
	]);

	const createComment = (title, text) => {
		const date = new Date();
		const revision = {
			created: date,
			tenantId: projectId,
			text,
			title,
			originalDate: date,
		};

		let lemmaCitation;
		// urn : cts : greekLit : tlg0012.tlg001.perseus-grc2 : 1.10@1-1.20@2
		if (typeof urn === 'string') {
			const [_nid, _nss, ctsNamespace, tg, passageCitation] = urn.split(':');
			const [textGroup, work, edition] = tg.split('.');
			const [_passageFrom, _passageTo = ''] = passageCitation.split('-');
			const [passageFrom, subreferenceIndexFrom] = _passageFrom.split('@');
			const [passageTo, subreferenceIndexTo] = _passageTo.split('@');

			lemmaCitation = {
				ctsNamespace,
				edition,
				textGroup,
				work,
				passageFrom: passageFrom.split('.').map(i => parseInt(i, 10)),
			};

			if (passageTo) {
				lemmaCitation.passageTo = passageTo
					.split('.')
					.map(i => parseInt(i, 10));
			} else {
				lemmaCitation.passageTo = passageFrom
					.split('.')
					.map(i => parseInt(i, 10));
			}

			if (subreferenceIndexFrom) {
				lemmaCitation.subreferenceIndexFrom = parseInt(
					subreferenceIndexFrom,
					10
				);
			}

			if (subreferenceIndexTo) {
				lemmaCitation.subreferenceIndexTo = parseInt(subreferenceIndexTo, 10);
			}
		}

		const comment = {
			originalDate: date,
			lemmaCitation,
			revisions: [revision],
			tenantId: projectId,
			urn: {
				v2: urn,
			},
		};

		return _createComment({
			variables: { comment },
		});
	};

	const updateComment = (title, text) => {
		const currentComment = Object.assign({}, comment);
		const date = new Date();
		const revision = {
			created: date,
			tenantId: projectId,
			title,
			text,
			originalDate: date,
		};
		currentComment.updated = date;
		currentComment.revisions = currentComment.revisions || [];
		currentComment.revisions.push(revision);

		delete currentComment.latestRevision;

		// This bug alone makes Apollo a huge pain
		delete currentComment.__typename;
		delete currentComment.lemmaCitation.__typename;

		return _updateComment({
			variables: { comment: currentComment },
		});
	};

	const handleSubmit = async e => {
		e.preventDefault();

		setSubmitting(true);

		let content;
		try {
			content = JSON.stringify(convertToRaw(localContent.getCurrentContent()));
		} catch (e) {
			console.error('Could not convert content to raw. Reason:', e);
			return;
		}

		// if the comment has already been saved on the
		// server, update it
		const result = Boolean(comment._id)
			? await updateComment(localTitle, content)
			: await createComment(localTitle, content);

		setSubmitting(false);

		return result;
	};
	const handleTitleChange = e => setLocalTitle(e.target.value);

	return (
		<div className="p1">
			<h3>{Boolean(comment._id) ? 'Edit Comment' : 'Create Comment'}</h3>
			<p className="gray focusText unselectable">{commentTarget}</p>
			{createCommentError && (
				<p className="error">{createCommentError.toString()}</p>
			)}
			{updateCommentError && (
				<p className="error">{updateCommentError.toString()}</p>
			)}
			{(createCommentLoading || updateCommentLoading) && <p>Loading...</p>}
			<form onSubmit={handleSubmit}>
				<div className="mb2">
					<input
						className="commentTitleInput border-0 f4 inherit-font m0 mb2 p0 w-100"
						onChange={handleTitleChange}
						placeholder="Title"
						value={localTitle}
					/>
					<OrpheusEditor
						editorState={localContent}
						enableInlineMedia
						handleChange={setLocalContent}
					/>
				</div>
				<div className="sidebarFormActionButtons">
					<FlatButton
						variant="contained"
						color="primary"
						onClick={handleSubmit}
						disabled={submitting}
					>
						{submitting ? ' Saving...' : 'Save'}
					</FlatButton>
					<FlatButton onClick={hideCommentForm}>Cancel</FlatButton>
				</div>
			</form>
		</div>
	);
};

CommentForm.propTypes = {
	comment: PropTypes.shape({
		_id: PropTypes.string,
		latestRevision: PropTypes.shape({
			originalDate: PropTypes.string,
			text: PropTypes.string,
			title: PropTypes.string,
		}),
	}),
	commentTarget: PropTypes.string,
	hideCommentForm: PropTypes.func,
	projectId: PropTypes.string,
	toggleSidePanel: PropTypes.func,
	urn: PropTypes.string,
};

export default CommentForm;
