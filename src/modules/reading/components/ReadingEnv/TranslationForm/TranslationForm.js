/**
 * @prettier
 */

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import FlatButton from 'material-ui/FlatButton';
import { EditorState, convertFromRaw, convertToRaw } from 'draft-js';
import { useMutation } from '@apollo/react-hooks';

import OrpheusEditor from '../../../../orpheus-editor/components/Editor';

import createUserTextMutation from '../../../graphql/mutations/createUserTextMutation';
import updateUserTextMutation from '../../../graphql/mutations/updateUserTextMutation';
import decorators from '../../../../orpheus-editor/components/decorators';

function TranslationForm({
	hideTranslationForm,
	projectId,
	textForTranslation = '',
	toggleSidePanel,
	translation = {},
	urn,
}) {
	// https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html#recommendation-fully-uncontrolled-component-with-a-key
	const revision = translation.latestRevision || {};
	const [localContent, setLocalContent] = useState(
		Boolean(revision.text)
			? EditorState.createWithContent(
				convertFromRaw(JSON.parse(revision.text)),
				decorators
			  )
			: EditorState.createEmpty()
	);
	const [submitting, setSubmitting] = useState(false);
	const [
		_createUserText,
		{
			data: createTranslationData,
			error: createTranslationError,
			loading: createTranslationLoading,
		},
	] = useMutation(createUserTextMutation, {
		refetchQueries: ['userTextsQuery'],
	});
	const [
		_updateUserText,
		{
			data: updateTranslationData,
			error: updateTranslationError,
			loading: updateTranslationLoading,
		},
	] = useMutation(updateUserTextMutation, {
		refetchQueries: ['userTextsQuery'],
	});

	useEffect(() => {
		if (createTranslationData || updateTranslationData) {
			hideTranslationForm();
		}

		// keep the panel open if the user is updating
		// a translation
		if (createTranslationData) {
			toggleSidePanel();
		}
	}, [createTranslationData, updateTranslationData]);

	const createTranslation = text => {
		const date = new Date();
		const revision = {
			created: date,
			originalDate: date,
			tenantId: projectId,
			text,
		};

		const userText = {
			commentaryID: projectId,
			contentType: 'translation',
			revisions: [revision],
			urn,
		};

		return _createUserText({
			variables: { userText },
		});
	};

	const updateTranslation = text => {
		const currentTranslation = Object.assign({}, translation);
		const date = new Date();
		const revision = {
			created: date,
			originalDate: date,
			tenantId: projectId,
			text,
		};

		currentTranslation.updated = date;
		currentTranslation.revisions = currentTranslation.revisions || [];
		currentTranslation.revisions.push(revision);

		delete currentTranslation.latestRevision;

		delete currentTranslation.__typename;

		return _updateUserText({
			variables: { userText: currentTranslation },
		});
	};

	const handleSubmit = async e => {
		e.preventDefault();

		setSubmitting(true);

		let content;
		try {
			content = JSON.stringify(convertToRaw(localContent.getCurrentContent()));
		} catch (e) {
			console.error('Could not convert content to raw. Reason:', e);
			return;
		}

		// if the translation has already been saved on the
		// server, update it
		const result = Boolean(translation._id)
			? await updateTranslation(content)
			: await createTranslation(content);

		setSubmitting(false);

		return result;
	};

	return (
		<div className="p1">
			<h3>
				{Boolean(translation._id) ? 'Edit Translation' : 'Create Translation'}
			</h3>
			<p className="gray focusText unselectable">{textForTranslation}</p>
			{createTranslationError && (
				<p className="error">{createTranslationError}</p>
			)}
			{updateTranslationError && (
				<p className="error">{updateTranslationError}</p>
			)}
			{(createTranslationLoading || updateTranslationLoading) && (
				<p>Loading...</p>
			)}
			<form onSubmit={handleSubmit}>
				<div className="mb2">
					<OrpheusEditor
						editorState={localContent}
						handleChange={setLocalContent}
					/>
				</div>
				<div className="sidebarFormActionButtons">
					<FlatButton
						variant="contained"
						color="primary"
						onClick={handleSubmit}
						disabled={submitting}
					>
						{submitting ? ' Saving...' : 'Save'}
					</FlatButton>
					<FlatButton onClick={hideTranslationForm}>Cancel</FlatButton>
				</div>
			</form>
		</div>
	);
}

TranslationForm.propTypes = {
	hideTranslationForm: PropTypes.func,
	textForTranslation: PropTypes.string,

	translation: PropTypes.shape({
		_id: PropTypes.string,
	}),
};

export default TranslationForm;
