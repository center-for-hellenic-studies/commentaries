/**
 * @prettier
 */

import React from 'react';
import { useQuery } from '@apollo/react-hooks';

import aboutThisTextQuery from '../../../graphql/queries/aboutThisText';

const SidePanelAbout = ({ urn }) => {
	const { loading, error, data } = useQuery(aboutThisTextQuery, {
		variables: { urn },
	});

	if (loading) {
		return <span>...</span>;
	}

	if (error) {
		return <span>Something went wrong, sorry. {error.message}</span>;
	}

	const work = data.TEXT_works[0];

	if (!work)
		return (
			<div>
				<span>Sorry, I didn't find anything for that URN.</span>
			</div>
		);

	return (
		<div>
			<h3>About this text</h3>
			<ul
				style={{
					listStyle: 'none',
					marginLeft: 0,
					paddingLeft: 0,
				}}
			>
				<li>Title: {work.original_title}</li>
				<li>Translations: English</li>
				<li>Genre: </li>
				<li>Themes: </li>
			</ul>
			<h4>Poetic form</h4>
			<ul
				style={{
					listStyle: 'none',
					marginLeft: 0,
					paddingLeft: 0,
				}}
			>
				<li>Meter: dactylic hexameter</li>
				<li>Source edition: {work.description}</li>
			</ul>
			<h4>Editorial principles</h4>
			<p>
				Quam multos scriptores rerum suarum magnus ille Alexander secum habuisse
				dicitur! Atque is tamen, cum in Sigeo ad Achillis tumulum astitisset: "O
				fortunate" inquit "adulescens, qui tuae virtutis Homerum praeconem
				inveneris!" Et vere. Nam nisi Illias illa exstitisset, idem tumulus, qui
				corpus eius contexerat, nomen etiam obruisset. Quid? noster hic Magnus,
				qui cum virtute fortunam adaequavit, nonne Theophanem Mytilenaeum,
				scriptorem rerum suarum, in contione militum civitate donavit; et nostri
				illi fortes viri, sed rustici ac milites, dulcedine quadam gloriae
				commoti, quasi participes eiusdem laudis, magno illud clamore
				approbaverunt? (Cic., <em>Pro Archia</em> 24)
			</p>
		</div>
	);
};

export default SidePanelAbout;
