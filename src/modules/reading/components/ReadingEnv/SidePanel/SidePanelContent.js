/**
 * @prettier
 */

import React, { useState } from 'react';
import {
	ContentState,
	convertFromHTML,
	convertFromRaw,
	EditorState,
} from 'draft-js';
import { useQuery } from '@apollo/react-hooks';

import Comment from '../Comment';
import SidePanelAbout from './SidePanelAbout';
import SidePanelHeader from './SidePanelHeader';
import SidePanelText from './SidePanelText';

import OrpheusEditor from '../../../../orpheus-editor/components/Editor';
import decorators from '../../../../orpheus-editor/components/decorators';

import projectCommentsQuery from '../../../graphql/queries/comments';
import userTextQuery from '../../../graphql/queries/userText';

import deriveLocationFromUrn from '../../../lib/deriveLocationFromUrn';
import { parseValueUrn as parseUrn } from '../../../lib/parseUrn';

const createEditorState = userTextNode => {
	const {
		latestRevision: { text },
	} = userTextNode;

	let baseContent = null;

	try {
		baseContent = convertFromRaw(JSON.parse(text));
	} catch (e) {
		const { contentBlocks, entityMap } = convertFromHTML(
			`<span>${text}</span>`
		);
		baseContent = ContentState.createFromBlockArray(contentBlocks, entityMap);
	}

	return EditorState.createWithContent(baseContent, decorators);
};

const renderUserTextNodes = (nodes = []) => {
	return nodes.map((n, i) => {
		const location = n.location || deriveLocationFromUrn(n.urn);
		const urn = n.urn;

		return (
			<div
				className="flex hide-child"
				key={`${n.urn}-${i}`}
				style={{ fontSize: '1.25rem', lineHeight: '1.6em' }}
			>
				<div
					className="child ease-in-out fw7 gray-dark left pointer unselectable"
					style={{ fontSize: 16, maxWidth: 70, width: 70 }}
				>
					{location.join('.')}
				</div>
				<div
					data-urn={urn}
					data-location={JSON.stringify(location)}
					style={{ flexGrow: 1 }}
				>
					<OrpheusEditor editorState={createEditorState(n)} readOnly />
				</div>
			</div>
		);
	});
};

function ProjectCommentary({
	hostname,
	openCommentForm,
	setCurrentComment,
	urn,
	work,
}) {
	const { data, error, loading } = useQuery(projectCommentsQuery, {
		variables: {
			hostname,
			urn,
		},
	});

	if (loading) {
		return <div>Loading commentary...</div>;
	}

	if (error) {
		return (
			<div className="error">
				<span>
					Sorry, we encountered an error loading the commentary. {error.message}
				</span>
			</div>
		);
	}

	const { project } = data;

	if (!project || !project.commentsOn || !project.commentsOn.length) {
		return null;
	}

	const { commentsOn: comments } = project;

	return (
		<div className="sidePanelContent">
			{comments.map(c => (
				<Comment
					comment={c}
					key={c._id}
					openCommentForm={openCommentForm}
					setCurrentComment={setCurrentComment}
					work={work}
				/>
			))}
		</div>
	);
}

function SidePanelTranslation({ urn }) {
	const { data, error, loading } = useQuery(userTextQuery, {
		fetchPolicy: 'no-cache',
		variables: {
			contentType: 'translation',
			search: urn,
		},
	});

	if (loading) {
		return <div>Loading translation...</div>;
	}

	if (error) {
		return (
			<div>
				<span className="error">
					Sorry, we encountered an error. {error.message}
				</span>
			</div>
		);
	}

	const { userTextMany: userTexts } = data;

	return renderUserTextNodes(userTexts);
}

const renderCurrentSelection = (
	currentSelection,
	baseUrn,
	work,
	setCurrentComment,
	openCommentForm
) => {
	if (!currentSelection) return null;

	if (currentSelection === '@@reading-env/about-the-text') {
		return <SidePanelAbout urn={baseUrn} />;
	}

	if (currentSelection === '@@reading-env/userText-translation') {
		return <SidePanelTranslation urn={baseUrn} />;
	}

	if (currentSelection.indexOf('@@project') === 0) {
		const [_, hostname] = currentSelection.split('/');

		return (
			<ProjectCommentary
				hostname={hostname}
				openCommentForm={openCommentForm}
				setCurrentComment={setCurrentComment}
				urn={baseUrn}
				work={work}
			/>
		);
	}

	const urn = currentSelection;
	const parsedUrn = parseUrn(urn) || {};
	if (parsedUrn.work) {
		return <SidePanelText urn={urn} work={work} />;
	}

	try {
		const nodes = JSON.parse(currentSelection);

		if (nodes && nodes.length) {
			return renderUserTextNodes(nodes);
		}
	} catch (e) {
		console.error(e.toString());
	}
};

const SidePanelContent = ({
	comments,
	openCommentForm,
	projectId,
	setCurrentComment,
	text,
}) => {
	const [currentSelection, setCurrentSelection] = useState('');

	const handleChange = e => setCurrentSelection(e.target.value);

	const { work } = text;
	const { urn: baseUrn } = work;

	return (
		<div className="sidePanelContentContainer">
			<div className="sidePanelHeader z5">
				<SidePanelHeader
					currentSelection={currentSelection}
					handleChange={handleChange}
					projectId={projectId}
					urn={baseUrn}
				/>
			</div>
			<div className="sidePanelContent">
				{renderCurrentSelection(
					currentSelection,
					baseUrn,
					work,
					setCurrentComment,
					openCommentForm
				)}
				{comments.map(comment => {
					return (
						<Comment
							comment={comment}
							key={comment._id}
							setCurrentComment={setCurrentComment}
							openCommentForm={openCommentForm}
							work={work}
						/>
					);
				})}
			</div>
		</div>
	);
};

export default SidePanelContent;
