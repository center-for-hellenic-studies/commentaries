/**
 * @prettier
 */

import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { useQuery } from '@apollo/react-hooks';

import editionsAndTranslationsQuery from '../../../graphql/queries/editionsAndTranslations';
import userProjectsQuery from '../../../graphql/queries/userProjects';

import {
	makeStyles,
	FormControl,
	FormHelperText,
	InputLabel,
	MenuItem,
	Select,
	Typography,
} from 'material-ui';

const renderCommentaries = (classes, projects) =>
	projects.map(p => {
		const { _id, hostname, title } = p;

		return (
			<MenuItem
				className={classes.item}
				key={_id}
				value={`@@project/${hostname}`}
			>
				<Typography noWrap>{title}</Typography>
			</MenuItem>
		);
	});

const renderWorks = (classes, works, workType = 'exemplar') =>
	works.map(w => {
		const { id, title, urn } = w[workType];

		return (
			<MenuItem className={classes.item} key={id} value={urn}>
				<Typography noWrap>{title}</Typography>
			</MenuItem>
		);
	});

const useStyles = makeStyles(theme => ({
	button: {
		margin: theme.spacing(1),
	},
	formControl: {
		display: 'flex',
		margin: theme.spacing(1),
		width: '100%',
	},
	group: {
		fontWeight: theme.typography.fontWeightMedium,
		opacity: 1,
	},
	item: {
		paddingLeft: 3 * theme.spacing(1),
		maxWidth: 600,
	},
}));

const SidePanelHeader = ({
	currentSelection,
	handleChange,
	projectId,
	urn,
}) => {
	const classes = useStyles();
	const inputLabel = useRef({});
	const formControl = useRef({});
	const [labelWidth, setLabelWidth] = useState(100);
	const { loading, error, data } = useQuery(editionsAndTranslationsQuery, {
		variables: { urn },
	});
	const {
		loading: projectsLoading,
		error: projectsError,
		data: projectsData,
	} = useQuery(userProjectsQuery);

	useEffect(() => {
		setLabelWidth(inputLabel.current.offsetWidth);
	}, []);

	if (loading) {
		return <FormControl className={classes.formControl} variant="outlined" />;
	}

	if (error) {
		return (
			<FormControl className={classes.formControl} variant="outlined">
				{error.graphQLErrors.map(({ message }, i) => (
					<FormHelperText key={i}>{message}</FormHelperText>
				))}
			</FormControl>
		);
	}

	const { TEXT_works: works } = data;
	const { editions, translations, versions } = works.reduce(
		(ws, w) => {
			if (w.exemplar) {
				ws.editions.push(w);
			} else if (w.translation) {
				ws.translations.push(w);
			} else if (w.version) {
				ws.versions.push(w);
			}

			return ws;
		},
		{ editions: [], translations: [], versions: [] }
	);

	return (
		<FormControl
			className={classes.formControl}
			ref={formControl}
			variant="filled"
		>
			<InputLabel
				htmlFor="textInfo"
				ref={inputLabel}
				id="@@reading-env/text-select-label"
			>
				Editions and translations
			</InputLabel>
			<Select
				inputProps={{ name: 'text-info', id: 'textInfo' }}
				labelId="@@reading-env/text-select-label"
				labelWidth={labelWidth}
				onChange={handleChange}
				value={currentSelection}
				variant="filled"
			>
				<MenuItem className={classes.item} value="@@reading-env/about-the-text">
					About the text
				</MenuItem>
				{/* renderCommentaries(classes, commentaries) */}
				{(editions.length > 0 || versions.length > 0) && (
					<MenuItem className={classes.group} disabled>
						Editions
					</MenuItem>
				)}
				{renderWorks(classes, editions, 'exemplar')}
				{renderWorks(classes, versions, 'version')}
				{translations.length > 0 && (
					<MenuItem className={classes.group} disabled>
						Translations
					</MenuItem>
				)}
				{renderWorks(classes, translations, 'translation')}
				<MenuItem
					className={classes.item}
					value="@@reading-env/userText-translation"
				>
					Your translation
				</MenuItem>
				{projectsData &&
					projectsData.userProjects &&
					projectsData.userProjects.length > 0 && (
						<MenuItem className={classes.group} disabled>
							Commentaries
						</MenuItem>
					)}
				{renderCommentaries(classes, projectsData.userProjects)}
			</Select>
		</FormControl>
	);
};

SidePanelHeader.propTypes = {
	currentSelection: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
	handleChange: PropTypes.func,
	urn: PropTypes.string,
};

export default SidePanelHeader;
