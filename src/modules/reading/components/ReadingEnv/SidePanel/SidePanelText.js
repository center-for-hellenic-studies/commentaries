/**
 * @prettier
 */

import React from 'react';
import { useQuery } from '@apollo/react-hooks';

import TextNode from '../TextNode';

import textNodesQuery from '../../../graphql/queries/textNodes';

import { parseValueUrn as parseUrn } from '../../../lib/parseUrn';
import getHeadings from '../../../lib/getHeadings';

const noop = () => {};

const SidePanelText = ({ urn, work }) => {
	let { passage } = parseUrn(urn);

	if (!passage) {
		const urnFromUrl = window.location.pathname.split('/')[2].split(/\?|\#/)[0];

		passage = (parseUrn(urnFromUrl) || {}).passage
	}

	let startsAtLocation = null;
	let endsAtLocation = null;
	if (passage && passage.length) {
		startsAtLocation = passage[0];
		if (passage.length > 1) {
			endsAtLocation = passage[1];
		}
	}
	const { loading, error, data } = useQuery(textNodesQuery, {
		variables: {
			urn,
			endsAtLocation,
			startsAtLocation,
		},
	});

	if (error || loading) {
		return (
			<div className="p4">
				{error && (
					<span className="error">
						Sorry, we encountered an error. {error.message}
					</span>
				)}
				{loading && <span>Loading text...</span>}
			</div>
		);
	}

	let textNodes = [];
	if (data.TEXT_workByUrn) {
		textNodes = data.TEXT_workByUrn.textNodes;
	}

	if (!textNodes.length) {
		return (
			<div>
				<span>Sorry, we didn't find anything for the URN {urn}</span>
			</div>
		);
	}

	const { refsDecls } = work;
	const text = textNodes.map((textNode, i) => {
		const prevNode = textNodes[i - 1];
		const prevLocation = (prevNode && prevNode.location) || [];
		const { location } = textNode;

		const { heading, subheading, subsubheading } = getHeadings(
			location,
			prevLocation,
			refsDecls
		);

		return (
			<div key={`div-${textNode.urn}-${textNode.location}`}>
				{heading}
				{subheading}
				{subsubheading}
				<TextNode
					hideInteractiveButtons
					key={`${textNode.urn}-${textNode.location}`}
					openCommentForm={noop}
					openSidePanel={noop}
					openSidePanelForUrn={noop}
					selection={null}
					setCurrentComment={noop}
					textNode={textNode}
				/>
			</div>
		);
	});

	return <div>{text}</div>;
};

export default SidePanelText;
