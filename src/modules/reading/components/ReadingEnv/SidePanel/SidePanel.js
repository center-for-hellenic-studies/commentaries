/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';

import CommentForm from '../CommentForm';
import SidePanelContent from './SidePanelContent';
import TranslationForm from '../TranslationForm';

const SidePanel = ({
	closeTranslationForm,
	comments,
	currentComment = {},
	currentTranslation = {},
	hideCommentForm,
	hideTranslationForm,
	openCommentForm,
	projectId,
	selectionText,
	selectionUrn,
	setCurrentComment,
	showCommentForm,
	showTranslationForm,
	sidePanelOpen,
	text,
	textNode,
	textNodes,
	toggleSidePanel,
}) => {
	const _hideCommentForm = e => {
		setCurrentComment(null);
		hideCommentForm(e);
	};

	return (
		<div className="sidePanelOuter">
			<div className="sidePanelHandle" onClick={toggleSidePanel}>
				<div className="sidePanelHandleBar">
					<div className="sidePanelHandleBarGrip"></div>
				</div>
			</div>
			{sidePanelOpen && (
				<div className="sidePanel">
					{showCommentForm && (
						<div className="sidePanelForm">
							<CommentForm
								comment={currentComment || {}}
								commentTarget={selectionText}
								hideCommentForm={_hideCommentForm}
								key={(currentComment || {})._id || selectionText}
								projectId={projectId}
								toggleSidePanel={toggleSidePanel}
								urn={selectionUrn}
							/>
						</div>
					)}
					{showTranslationForm && (
						<div className="sidePanelForm">
							<TranslationForm
								hideTranslationForm={hideTranslationForm}
								key={(currentTranslation || {})._id || selectionText}
								projectId={projectId}
								textForTranslation={selectionText}
								toggleSidePanel={toggleSidePanel}
								translation={currentTranslation || {}}
								urn={selectionUrn}
							/>
						</div>
					)}
					{!showCommentForm && !showTranslationForm && (
						<SidePanelContent
							comments={comments}
							openCommentForm={openCommentForm.bind(null, [textNode])}
							projectId={projectId}
							setCurrentComment={setCurrentComment}
							text={text}
							textNode={textNode}
							textNodes={textNodes}
						/>
					)}
				</div>
			)}
		</div>
	);
};

SidePanel.propTypes = {
	closeTranslationForm: PropTypes.func,
	comments: PropTypes.array,
	currentComment: PropTypes.shape({
		_id: PropTypes.string,
		latestRevision: PropTypes.shape({
			text: PropTypes.string,
			title: PropTypes.string,
		}),
	}),
	hideCommentForm: PropTypes.func,
	openCommentForm: PropTypes.func,
	projectId: PropTypes.string,
	selectionText: PropTypes.string,
	setActiveComment: PropTypes.func,
	setCurrentComment: PropTypes.func,
	showCommentForm: PropTypes.bool,
	sidePanelOpen: PropTypes.bool,
	text: PropTypes.object,
	textNode: PropTypes.object,
	textNodes: PropTypes.array,
	toggleSidePanel: PropTypes.func,
};

export default SidePanel;
