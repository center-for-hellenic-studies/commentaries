/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import {
	ContentState,
	convertFromHTML,
	convertFromRaw,
	EditorState,
} from 'draft-js';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import EditIcon from '@material-ui/icons/Edit';
import ShareIcon from '@material-ui/icons/Share';
import { useMutation } from '@apollo/react-hooks';

import OrpheusEditor from '../../../orpheus-editor/components/Editor';
import decorators from '../../../orpheus-editor/components/decorators';

import removeCommentMutation from '../../graphql/mutations/removeCommentMutation';

import markCitations from '../../lib/markCitations';

Comment.propTypes = {
	comment: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		latestRevision: PropTypes.shape({
			created: PropTypes.string,
			text: PropTypes.string,
			title: PropTypes.title,
		}).isRequired,
		urn: PropTypes.shape({
			v1: PropTypes.string,
			v2: PropTypes.string,
		}),
		user: PropTypes.object,
	}),
	openCommentForm: PropTypes.func.isRequired,
	setCurrentComment: PropTypes.func.isRequired,
	work: PropTypes.shape({
		textNodes: PropTypes.array,
	}),
};

function Comment({ comment, openCommentForm, setCurrentComment, work }) {
	const [removeComment, _removeCommentResponse] = useMutation(
		removeCommentMutation,
		{
			refetchQueries: ['projectCommentsQuery'],
		}
	);

	const _editComment = comment => e => {
		setCurrentComment(comment);
		openCommentForm(e);
	};

	const _deleteComment = comment => e => {
		e.preventDefault();
		e.stopPropagation();

		if (window.confirm('Are you sure you want to delete this comment?')) {
			removeComment({ variables: { _id: comment._id } });
		}
	};

	const _shareComment = comment => e => {
		// todo share comment;
		console.log(comment);
	};

	const revision = comment.latestRevision;
	if (!revision) {
		return;
	}

	// FIXME: (charles) Should this just use comment.urn.v2?
	let urn = '';

	if (work && work.textNodes) {
		let textNodes = work.textNodes;
		let locationLength = textNodes[0].location.length;
		let lemmaCitation = comment.lemmaCitation;
		urn = `urn:cts:${lemmaCitation.ctsNamespace}:${lemmaCitation.textGroup}.${
			lemmaCitation.work
		}:${lemmaCitation.passageFrom.slice(0, locationLength)}`;
	}

	// mark citations in text
	// can be added to orpheus editor with predictive citation model in the future
	const revisionText = markCitations(revision.text);

	let baseContent;
	try {
		baseContent = convertFromRaw(JSON.parse(revisionText));
	} catch (e) {
		const { contentBlocks, entityMap } = convertFromHTML(revisionText);
		baseContent = ContentState.createFromBlockArray(contentBlocks, entityMap);
	}

	return (
		<div
			className="comment"
			key={comment._id}
			data-commentid={comment._id}
			data-urn={urn}
		>
			<div className="commentHead">
				<div className="commentHeadText">
					{revision.title ? <h2>{revision.title}</h2> : ''}
					<span className="commentByline">
						{new Date(revision.created).toLocaleDateString()} by
						<a href="/commenters/gregory-nagy" className="commenterLink">
							<div
								className="chipAvatar"
								style={{
									backgroundImage:
										'url("https://archimedes-data003.s3-us-west-2.amazonaws.com/FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg")',
								}}
							/>

							<span>Gregory Nagy</span>
						</a>
					</span>
				</div>
				<div className="commentActions flex">
					<span
						role="button"
						className="dark-gray hover-opaque ml2 mr2 muted pointer"
						onClick={_shareComment(comment)}
					>
						<ShareIcon />
					</span>
					<span
						role="button"
						className="dark-gray hover-opaque mr2 muted pointer"
						onClick={_editComment(comment)}
					>
						<EditIcon />
					</span>
					<span
						role="button"
						className="dark-gray hover-opaque muted pointer"
						onClick={_deleteComment(comment)}
					>
						<DeleteOutlineIcon />
					</span>
				</div>
			</div>
			<div className="flex">
				{/* <span
									className="gray hover-opaque ml1 muted pointer"
									onClick={_highlightComment(comment)}
								>
									<VisibilityIcon />
								</span> */}
				<div className="commentBody">
					<OrpheusEditor
						editorState={EditorState.createWithContent(baseContent, decorators)}
						readOnly
					/>
				</div>
			</div>
		</div>
	);
}

export default Comment;
