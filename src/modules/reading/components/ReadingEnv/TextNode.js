/**
 * @prettier
 */

import React, { createRef, useEffect, useLayoutEffect, useState } from 'react';
import PropTypes from 'prop-types';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import EditIcon from '@material-ui/icons/Edit';
import ModeCommentTwoToneIcon from '@material-ui/icons/ModeCommentTwoTone';
import PublishOutlinedIcon from '@material-ui/icons/PublishOutlined';
import TranslateIcon from '@material-ui/icons/Translate';
import {
	ContentState,
	EditorState,
	convertFromHTML,
	convertFromRaw,
	convertToRaw,
} from 'draft-js';
import { useMutation } from '@apollo/react-hooks';
import cn from 'classnames';

import OrpheusEditor from '../../../orpheus-editor/components/Editor';
import decorators from '../../../orpheus-editor/components/decorators';

import createUserTextMutation from '../../graphql/mutations/createUserTextMutation';
import updateUserTextMutation from '../../graphql/mutations/updateUserTextMutation';

import deriveLocationFromUrn from '../../lib/deriveLocationFromUrn';

const noop = () => {};

const withoutPassageCitation = (urn = '') =>
	urn
		.split(':')
		.slice(0, 4)
		.join(':');

const getInnerText = html => {
	const div = document.createElement('div');

	div.innerHTML = html;

	return div.innerText;
};

const renderCommentsIcon = (comments = [], onClick = () => {}) => {
	const numComments = comments.length;
	const classes = cn(
		'f2 flex flex-column items-center pointer relative unselectable',
		{
			hidden: numComments === 0,
		}
	);
	return (
		<div className={classes} onClick={onClick}>
			<div
				className="absolute left unselectable z2 commentIconCount"
				style={{
					fontWeight: 'bold',
					fontSize: 12,
					left: '50%',
					paddingBottom: 4,
					top: '50%',
					transform: 'translate(-50%, -50%)',
					fontFamily:
						'-apple-system, BlinkMacSystemFont, Helvetica Neue, Helvetica, Liberation Sans, Noto, sans-serif',
				}}
			>
				{numComments}
			</div>
			<ModeCommentTwoToneIcon fontSize="inherit" />
		</div>
	);
};

const renderEditingIcon = (
	editing,
	classes,
	handleEditClick,
	handleCancelClick,
	handleSubmitClick
) => {
	if (editing) {
		return (
			<React.Fragment>
				<div className={cn(classes, 'mr1')} onClick={handleSubmitClick}>
					<PublishOutlinedIcon />
				</div>
				<div className={cn(classes, 'mr1')} onClick={handleCancelClick}>
					<CancelOutlinedIcon />
				</div>
			</React.Fragment>
		);
	}

	return (
		<div className={cn(classes, 'mr1')} onClick={handleEditClick}>
			<EditIcon />
		</div>
	);
};

const UserEditionTextNode = ({
	editing,
	editorState,
	handleCancel,
	handleSubmit,
	setEditorState,
}) => {
	const editorNode = createRef();

	useEffect(() => {
		if (editing) {
			try {
				editorNode.current.focus();
			} catch (e) {
				console.error('Failed to focus OrpheusEditor. Reason:', e.toString());
			}
		}
	}, [editing]);

	const classes = cn({
		outline: editing,
		p1: editing,
	});

	return (
		<div className={classes}>
			<OrpheusEditor
				editorState={editorState}
				handleChange={setEditorState}
				readOnly={!editing}
				ref={editorNode}
			/>
		</div>
	);
};

UserEditionTextNode.propTypes = {
	editing: PropTypes.bool,
	handleCancel: PropTypes.func,
	handleSubmit: PropTypes.func,
	userText: PropTypes.shape({
		latestRevision: PropTypes.shape({
			text: PropTypes.string,
		}),
	}),
	textNode: PropTypes.shape({
		text: PropTypes.string,
	}),
};

const TextNode = ({
	comments,
	hideInteractiveButtons,
	openSidePanel,
	openSidePanelForTranslation,
	openSidePanelForUrn,
	projectId,
	setSelectedComments,
	textNode,
	urn,
	userText = {},
}) => {
	const { text } = textNode;
	const location = textNode.location || deriveLocationFromUrn(urn);
	urn = `${withoutPassageCitation(urn)}:${location.join('.')}`;
	const edition =
		userText.contentType === 'edition' ? userText.latestRevision : {};
	const innerText = getInnerText(text);
	const [documentHasSelection, setDocumentHasSelection] = useState(false);
	const [editing, setEditing] = useState(false);
	let baseContent;
	if (Boolean(edition.text)) {
		try {
			baseContent = convertFromRaw(JSON.parse(edition.text));
		} catch (e) {
			const { contentBlocks, entityMap } = convertFromHTML(
				`<span>${edition.text}</span>`
			);
			baseContent = ContentState.createFromBlockArray(contentBlocks, entityMap);
		}
	} else {
		const { contentBlocks, entityMap } = convertFromHTML(
			`<span>${text}</span>`
		);
		baseContent = ContentState.createFromBlockArray(contentBlocks, entityMap);
	}
	const baseEditorState = EditorState.createWithContent(
		baseContent,
		decorators
	);
	const [editorState, setEditorState] = useState(baseEditorState);
	const [submitting, setSubmitting] = useState(false);
	const [
		_createUserText,
		{
			data: createEditionData,
			error: createEditionError,
			loading: createEditionLoading,
		},
	] = useMutation(createUserTextMutation);
	const [
		_updateUserText,
		{
			data: updateEditionData,
			error: updateEditionError,
			loading: updateEditionLoading,
		},
	] = useMutation(updateUserTextMutation);

	useEffect(() => {
		if (createEditionData || updateEditionData) {
			setEditing(false);
		}
	}, [createEditionData, updateEditionData]);

	useLayoutEffect(() => {
		setDocumentHasSelection(!window.getSelection().isCollapsed);
	}, [window.getSelection().isCollapsed]);

	const createEdition = text => {
		const date = new Date();
		const revision = {
			created: date,
			originalDate: date,
			tenantId: projectId,
			text,
		};

		const userText = {
			commentaryID: projectId,
			contentType: 'edition',
			revisions: [revision],
			urn,
		};

		return _createUserText({
			variables: { userText },
		});
	};

	const updateEdition = text => {
		const currentEdition = Object.assign({}, userText);
		const date = new Date();
		const revision = {
			created: date,
			originalDate: date,
			tenantId: projectId,
			text,
		};

		currentEdition.updated = date;
		currentEdition.revisions = currentEdition.revisions || [];
		currentEdition.revisions.push(revision);

		currentEdition.revisions.forEach(r => {
			delete r.__typename;
		});
		delete currentEdition.latestRevision;
		delete currentEdition.__typename;
		delete currentEdition.updated;

		return _updateUserText({
			variables: { record: currentEdition },
		});
	};

	const handleCommentClick = e => {
		e.preventDefault();

		openSidePanelForUrn(urn, edition.text || innerText);
	};

	const handleCancelEdit = e => {
		e.preventDefault();

		setEditing(false);
	};

	const handleEditClick = e => {
		e.preventDefault();

		setEditing(true);
	};

	const handleSubmitEdit = async e => {
		e.preventDefault();

		setSubmitting(true);

		let content;
		try {
			content = JSON.stringify(convertToRaw(editorState.getCurrentContent()));
		} catch (e) {
			console.error('Could not convert content to raw. Reason:', e);
			return;
		}

		const result = Boolean(edition._id)
			? await updateEdition(content)
			: await createEdition(content);

		setSubmitting(false);

		return result;
	};

	const handleTranslateClick = e => {
		e.preventDefault();

		openSidePanelForTranslation(urn, edition.text || innerText);
	};

	const showCommentsForNode = e => {
		setSelectedComments(comments);
		openSidePanel();
	};

	const inlineButtonClasses = cn(
		'child ease-in-out fw7 gray-dark pointer unselectable mr1',
		{
			hidden: documentHasSelection,
		}
	);
	const commentButtonClasses = cn(
		'ease-in-out fw7 pointer unselectable commentButton',
		{
			hidden: documentHasSelection,
		}
	);

	const textClasses = cn('textNode w-80', {
		indent: text.indexOf('<milestone') > -1,
	});

	return (
		<div
			className="flex hide-child"
			style={{ fontSize: '1.25rem', lineHeight: '1.6em' }}
		>
			<div className="flex flex-row items-center textNodeLocation">
				<div
					className="child ease-in-out fw7 gray-dark left pointer unselectable"
					onClick={hideInteractiveButtons ? noop : handleCommentClick}
					role="button"
					style={{ maxWidth: 70, width: 70 }}
				>
					{location.join('.')}
				</div>
			</div>
			<div
				className={textClasses}
				data-urn={urn}
				data-location={JSON.stringify(location)}
				style={{ flexGrow: 1 }}
			>
				<UserEditionTextNode
					editing={editing}
					editorState={editorState}
					handleCancel={handleCancelEdit}
					handleSubmit={handleSubmitEdit}
					setEditorState={setEditorState}
				/>
			</div>
			{hideInteractiveButtons ? null : (
				<div
					className="flex flex-row items-center px1 relative"
					style={{ top: 4 }}
				>
					{renderEditingIcon(
						editing,
						inlineButtonClasses,
						handleEditClick,
						handleCancelEdit,
						handleSubmitEdit
					)}
					<div
						className={inlineButtonClasses}
						onClick={handleTranslateClick}
						role="button"
					>
						<TranslateIcon />
					</div>
					<div className={commentButtonClasses}>
						{renderCommentsIcon(comments, showCommentsForNode)}
					</div>
				</div>
			)}
		</div>
	);
};

TextNode.propTypes = {
	hideInteractiveButtons: PropTypes.bool,
	openSidePanel: PropTypes.func,
	openSidePanelForTranslation: PropTypes.func,
	openSidePanelForUrn: PropTypes.func,
	projectId: PropTypes.string,
	setSelectedComments: PropTypes.func,
	textNode: PropTypes.shape({
		location: PropTypes.array,
		text: PropTypes.string,
		urn: PropTypes.string,
	}).isRequired,
	userText: PropTypes.shape({
		latestRevision: PropTypes.shape({
			text: PropTypes.string,
		}),
	}),
};

export default TextNode;
