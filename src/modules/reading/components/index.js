import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.hydrate(
	<App text={JSON.parse(window.__REACT_DATA__).text} />,
	document.getElementById('@@react-app')
);
