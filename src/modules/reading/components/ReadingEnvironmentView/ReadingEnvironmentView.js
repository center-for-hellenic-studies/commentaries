import { hot } from 'react-hot-loader/root';

import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import Routes from '../../routes';
import Root from '../../../../containers/Root';

const ReadingEnvironmentView = () => (
	<Root>
		<Router>
			{Routes}
		</Router>
	</Root>
);

export default hot(ReadingEnvironmentView);
