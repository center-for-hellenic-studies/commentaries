/**
 * @prettier
 */

import gql from 'graphql-tag';

const TEXT_NODE_FRAGMENT = gql`
	fragment AppTextNode on TEXT_TextNode {
		id
		index
		location
		text
		urn
	}
`;

const USER_TEXT_FRAGMENT = gql`
	fragment AppUserText on UserText {
		_id
		commentaryID
		contentType
		createdAt
		latestRevision {
			_id
			created
			tenantId
			text
			title
		}
		revisions {
			_id
			created
			createdBy
			originalDate
			tenantId
			text
			title
		}
		updatedAt
		urn
		userID
	}
`;

const APP_INIT_QUERY = gql`
	query appInitQuery(
		$commentaryID: String
		$endsAtLocation: [Int]
		$fullUrnWithoutPassageCitation: String!
		$startsAtLocation: [Int]
		$urn: TEXT_CtsUrn
		$userTextContentType: EnumUserTextContentType
		$userTextSearch: String
		$userTextStatus: EnumUserTextStatus
	) {
		TEXT_textGroups(urn: $urn) {
			id
			title
		}

		TEXT_workByUrn(full_urn: $fullUrnWithoutPassageCitation) {
			id
			slug
			english_title
			original_title
			form
			work_type
			label
			description
			urn
			full_urn
			language {
				id
				title
				slug
			}
			version {
				id
				slug
				title
				description
			}
			exemplar {
				id
				slug
				title
				description
			}
			refsDecls {
				description
				label
			}
			translation {
				id
				slug
				title
				description
			}
			textNodes(
				startsAtLocation: $startsAtLocation
				endsAtLocation: $endsAtLocation
			) {
				...AppTextNode
			}
		}

		userTextMany(
			filter: {
				commentaryID: $commentaryID
				contentType: $userTextContentType
				search: $userTextSearch
				status: $userTextStatus
			}
		) {
			...AppUserText
		}
	}
	${TEXT_NODE_FRAGMENT}
	${USER_TEXT_FRAGMENT}
`;

export default APP_INIT_QUERY;
