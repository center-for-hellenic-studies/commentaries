/**
 * @prettier
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Reader from './ReadingEnv/Reader';
import SidePanel from './ReadingEnv/SidePanel';
import classnames from 'classnames';
import { useQuery } from '@apollo/react-hooks';

import APP_INIT_QUERY from './App.graphql';

import projectCommentsQuery from '../graphql/queries/comments';

import { parseValueUrn as parseUrn } from '../lib/parseUrn';

const textGroupUrn = (urn = '') => (urn.split('.') || [])[0];
const withoutPassageCitation = (urn = '') =>
	urn
		.split(':')
		.slice(0, 4)
		.join(':');

const App = ({ urn }) => {
	const [commentFormOpen, setCommentFormOpen] = useState(false);
	const [currentComment, setCurrentComment] = useState({});
	const [currentTranslation, setCurrentTranslation] = useState({});
	const [selectedComments, setSelectedComments] = useState([]);
	const [selectionText, setSelectionText] = useState('');
	const [selectionUrn, setSelectionUrn] = useState('');
	const [sidePanelOpen, setSidePanelOpen] = useState(false);
	const [translationFormOpen, setTranslationFormOpen] = useState(false);

	const { passage } = parseUrn(urn);
	let startsAtLocation = null;
	let endsAtLocation = null;
	if (passage && passage.length) {
		startsAtLocation = passage[0];
		if (passage.length > 1) {
			endsAtLocation = passage[1];
		}
	}
	const { loading, error, data } = useQuery(APP_INIT_QUERY, {
		variables: {
			fullUrn: urn,
			fullUrnWithoutPassageCitation: withoutPassageCitation(urn),
			commentsLimit: 1000,
			startsAtLocation,
			endsAtLocation,
			urn: textGroupUrn(urn),
			userTextContentType: 'edition',
			userTextSearch: withoutPassageCitation(urn),
		},
	});
	const {
		loading: commentsLoading,
		error: commentsError,
		data: commentsData,
	} = useQuery(projectCommentsQuery, {
		variables: { urn, limit: 1000 },
	});
	const closeCommentForm = _e => setCommentFormOpen(false);
	const hideTranslationForm = _e => setTranslationFormOpen(false);
	const openCommentForm = _e => {
		hideTranslationForm();
		setCommentFormOpen(true);
	};
	const openTranslationForm = _e => {
		closeCommentForm();
		setTranslationFormOpen(true);
	};
	const openSidePanel = _e => setSidePanelOpen(true);
	const openSidePanelForUrn = (urn, text = '') => {
		setSelectionText(text);
		setSelectionUrn(urn);
		openCommentForm();
		setSidePanelOpen(true);
	};
	const openSidePanelForTranslation = (urn, text = '') => {
		setSelectionText(text);
		setSelectionUrn(urn);
		openTranslationForm();
		setSidePanelOpen(true);
	};
	const toggleSidePanel = _e => setSidePanelOpen(!sidePanelOpen);

	if (loading) {
		return (
			<div className="ReadingEnv">
				<div className="readingEnvLoadingContent">
					<div className="readingEnvLoadingHeader" />
					<div className="readingEnvLoadingLine" />
					<div className="readingEnvLoadingLine" />
					<div className="readingEnvLoadingLine" />
					<div className="readingEnvLoadingLine" />
					<div className="readingEnvLoadingLine" />
				</div>
			</div>
		);
	}

	if (error) {
		return (
			<div>
				<span className="error">We encountered an error: {error.message}</span>
			</div>
		);
	}

	if (commentsError) {
		console.error('Error loading comments: ', error);
	}

	const {
		TEXT_workByUrn: work = {},
		TEXT_textGroups: textGroups = [],
		userTextMany: userTexts = [],
	} = data;

	const textGroup = textGroups[0];

	if (work) {
		work.title = work.original_title || work.english_title;
	}

	let comments = [];
	if (commentsData && commentsData.project) {
		comments = commentsData.project.commentsOn;

		if (!comments.length) {
			console.warn('No comments were found on that project.');
		}
	}

	const text = { work, textGroup };
	const textNodes = work.textNodes;

	const classes = classnames('ReadingEnv', {
		sidePanelVisible: sidePanelOpen,
	});

	return (
		<div className={classes}>
			<Reader
				comments={comments}
				openCommentForm={openCommentForm}
				openSidePanelForTranslation={openSidePanelForTranslation}
				openSidePanelForUrn={openSidePanelForUrn}
				openSidePanel={openSidePanel}
				selectedComments={selectedComments}
				setSelectedComments={setSelectedComments}
				text={text}
				textNodes={textNodes}
				urn={urn}
				userTexts={userTexts}
			/>
			<SidePanel
				comments={selectedComments}
				currentComment={currentComment}
				currentTranslation={currentTranslation}
				hideCommentForm={closeCommentForm}
				hideTranslationForm={hideTranslationForm}
				openCommentForm={openCommentForm}
				selectionText={selectionText}
				selectionUrn={selectionUrn}
				setCurrentComment={setCurrentComment}
				setCurrentTranslation={setCurrentTranslation}
				showCommentForm={commentFormOpen}
				showTranslationForm={translationFormOpen}
				sidePanelOpen={sidePanelOpen}
				text={text}
				textNodes={textNodes}
				toggleSidePanel={toggleSidePanel}
				urn={urn}
				userTexts={userTexts}
			/>
		</div>
	);
};

App.propTypes = {
	urn: PropTypes.string.isRequired,
};

export default App;
