Alexandria Reading Environment
------

## GraphQL

The DOM structure tries its best to mirror the data structure coming from the server. Roughly,
that means that we have three top-level nodes:

1. `TEXT_work`: basic information about the "work" in question.
2. `TEXT_textGroup`: basic information about the work's source (often, but not always, its author)
3. `TEXT_textNodes`: the work's document (i.e., the text itself)

`TEXT_work` and `TEXT_textGroup` primarily populate the `<Header>` and `<SidePanel>` components.
`TEXT_textNodes` are used for the `<Reader>` and, under certain circumstances, `<SidePanelContent>`
components.

### CTS URNs

One of the main challenges with getting the correct data is keeping track of how the server treats
[CTS URNs](http://cite-architecture.org/ctsurn/overview/). Work is being done to offload URN
parsing/serializing to the server, but for now it is important to remember that 

1. The `textDetailQuery`
expects both a `$full_urn: String` variable and a `$short_urn: TEXT_CtsUrn` variable. The variable names
are not helpful: both are technically complete CTS URNs, but the `$full_urn` contains information about
the desired edition/translation (e.g., `urn:cts:greekLit:tlg0548.tlg001.perseus-grc2`), while the 
`$short_urn` points more concisely to the general "work" (implying all of its editions/translations;
e.g., `urn:cts:greekLit:tlg0548.tlg001`). 

2. The `textNodes` query, by contrast, expects a `$urn: TEXT_CtsUrn!` variable, usually one that 
specifies a range of `TEXT_textNodes`. So for the sections 1.1.1 to 2.1.1 (inclusive) of Apollodorus'
_Library_, we would use `urn:cts:greekLit:tlg0548.tlg001.perseus-grc2:1.1.1-2.1.1`.

## Hot-reloading

So far, this is the only part of the Alexandria client-side application to support
true hot-reloading, i.e., where changes to component structure are reflected without
losing state (and without a full page refresh). Key pieces of this architecture are:

### 1. The ReadingEnvironmentView

This is the entrypoint for the ReadingEnv application. We mark it as hot-exported using
`react-hot-loader`:

```javascript
// components/ReadingEnvironmentView/ReadingEnvironmentView.js

import { hot } from 'react-hot-loader/root';

import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import Routes from '../../routes';
import Root from '../../../../containers/Root';

const ReadingEnvironmentView = () => (
  <Root>
    <Router>
      {Routes}
    </Router>
  </Root>
);

export default hot(ReadingEnvironmentView);
```

### 2. The Webpack config

We've added an entrypoint to the webpack.dev.config.js, which lets us avoid awkwardly making sure that
`react-hot-loader` is always imported before `react` and `react-dom` in the application code.

```javascript
{
    // ...
    entry: [
		'react-hot-loader/patch',
		paths.appIndexJs,
    ],
    // ...
}
```

We've also aliased `react-dom` to `@hot-loader/react-dom`. While we've only made this change in development,
it would be fine for production as well --- `@hot-loader/react-dom` is a small wrapper over `react-dom` that
is just a no-op in production.

```javascript
{
    // ...
    alias: {
        'react-native': 'react-native-web',
        'react-dom': '@hot-loader/react-dom',
    },
    // ...
}
```

Finally, we've added a plugin to the `babel-loader` section of the Webpack config.

```javascript
{
    // ...
    // Process application JS with Babel.
    // The preset includes JSX, Flow, TypeScript, and some ESnext features.
    {
        test: /\.(js|jsx|ts|tsx)$/,
        include: paths.appSrc,
        loader: require.resolve('babel-loader'),
        options: {
            customize: require.resolve(
                'babel-preset-react-app/webpack-overrides'
            ),

            plugins: [
                'react-hot-loader/babel', // <-- plugin in question
                // ...
            ],
            cacheDirectory: true,
            cacheCompression: false,
            compact: false,
        },
    },
}
```