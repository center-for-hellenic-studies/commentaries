const commenters = {
	homer: [
		{
			"_id": "L7dkgWKWrQ2FCQb9B",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Anita Nikkanen",
			"slug": "anita-nikkanen",
			"bio": "<p>Anita Nikkanen is a Research Fellow in Classical Philology at the Center for Hellenic Studies. Her research focuses on Homeric epic and Augustan poetry, and she has published on Ovid, Propertius, Tibullus, Virgil, and the Homeric Odyssey.</p>\"",
			"isAuthor": null,
			"tagline": "Anita Nikkanen is a Research Fellow in Classical Philology at the Center for Hellenic Studies",
			"featureOnHomepage": false,
			"avatar": {
				"src": "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ / anita_nikkanen.jpeg",
				"filename": "anita_nikkanen.jpeg",
				"type": "image/jpeg",
				"size": 16920,
				"directive": "uploads",
				"key": "JnSr8x5kfWvmeraLZ / anita_nikkanen.jpeg"
			},
			"nCommentsTotal": 17
		},
		{
			"_id": "xEN3etPuAjh2FxrQq",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Casey Dué",
			"slug": "casey-due",
			"bio": "<p>Casey Dué is Professor and Director of Classical Studies at the University of Houston. Publications include Homeric Variations on a Lament by Briseis (Lanham, MD, 2002), The Captive Woman’s Lament in Greek Tragedy (Austin, 2006) and (co-authored with Mary Ebbott) Iliad 10 and the Poetics of Ambush (Washington, DC, 2010). She is co-editor with Mary Ebbott of the Homer Multitext project (<a href='http://www.homermultitext.org' target='_blank'>http://www.homermultitext.org</a>).</p>",
			"isAuthor": null,
			"tagline": "Casey Dué is Professor and Director of Classical Studies at the University of Houston",
			"featureOnHomepage": false,
			"avatar": {
				"src": "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ / casey_due.jpg",
				"filename": "casey_due.jpg",
				"type": "image/jpeg",
				"size": 98951,
				"directive": "uploads",
				"key": "JnSr8x5kfWvmeraLZ / casey_due.jpg"
			},
			"nCommentsTotal": 0
		},
		{
			"_id": "gxHafEZzEqFWLZmj9",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Corinne Pache",
			"slug": "corinne-pache",
			"bio": "<p>Corinne Pache is a Professor in the Classical Studies department at Trinity, where she started teaching in the fall of 2009. Her research focuses on Greek archaic poetry and the modern reception of ancient epic. Recent publications include\"'Go Back to Your Loom Dad': Nostos in the 21st Century,\" in Odyssean Identities in Modern Cultures: the Journey Home, edited by Hunter Gardner and Sheila Murnaghan (Ohio State University Press, 2014), and  “Mourning Lions and Penelope’s Revenge, ” in the journal Arethusa (2016).</p>",
			"isAuthor": null,
			"tagline": "Corinne Pache is a Professor in the Classical Studies department at Trinity, where she started teaching in the fall of 2009",
			"featureOnHomepage": null,
			"nCommentsTotal": 0
		},
		{
			"_id": "L793yeLoDi6EZgAuP",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "David Elmer",
			"slug": "david-elmer",
			"bio": "<p>David F. Elmer is a Professor of the Classics at Harvard University. His recent publications include “The ‘Narrow Road’ and the Ethics of Language Use in the Iliad and Odyssey,” in volume 44 of <em>Ramus</em> (2015), and The Poetics of Consent: Collective Decision Making and the Iliad (<em>Johns Hopkins UP</em>, 2013).</p>",
			"isAuthor": null,
			"tagline": "David F. Elmer is a Professor of the Classics at Harvard University. His recent publications include The ‘Narrow Road’",
			"featureOnHomepage": false,
			"avatar": {
				"src": "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ / david_elmer.jpeg",
				"filename": "david_elmer.jpeg",
				"type": "image/jpeg",
				"size": 76908,
				"directive": "uploads",
				"key": "JnSr8x5kfWvmeraLZ / david_elmer.jpeg"
			},
			"nCommentsTotal": 4
		},
		{
			"_id": "KXtp8q8tA8wqQhqo6",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Douglas Frame",
			"slug": "douglas-frame",
			"bio": "<p>1971 Harvard Ph.D. in classical philology; until 2000 college teaching for some of the time, other pursuits for more of the time; since 2000 Associate Director of the Center for Hellenic Studies in Washington; after retiring as Associate Director in 2012 continued affiliation with the CHS.</p>",
			"isAuthor": null,
			"tagline": "1971 Harvard Ph.D. in classical philology; until 2000 college teaching for some of the time, other pursuits for more of the time",
			"featureOnHomepage": true,
			"avatar": {
				"src": "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ / douglas_frame.png",
				"filename": "douglas_frame.png",
				"type": "image/png",
				"size": 181849,
				"directive": "uploads",
				"key": "JnSr8x5kfWvmeraLZ / douglas_frame.png"
			},
			"nCommentsTotal": 802
		},
		{
			"_id": "ZRdE3MA2suY6jjY5G",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Gregory Nagy",
			"slug": "gregory-nagy",
			"bio": "<p>Born in Budapest, Hungary, Professor Nagy was educated at Indiana University and Harvard University, where he studied Classical Philology and Linguistics, receiving his Ph.D. in 1966. He has held positions at Johns Hopkins University and since 1975 in Harvard University, where he was named in 1984 the Francis Jones Professor of Classical Greek Literature and Professor of Comparative Literature. He is also currently the Curator of the Milman Parry Collection of Oral Literature and, since August of 2000, the Director of Harvard University's Center for Hellenic Studies. Professor Nagy has served as Chair of the Harvard University Classics Department and as President of the American Philological Association.</p><p>Professor Nagy is a renowned authority in the field of Homeric and related Greek studies. His numerous honors include a Guggenheim Fellowship and the Goodwin Award of Merit of the American Philological Association for his book, The Best of the Achaeans (1979). In addition to this path-breaking work, he has published Greek Dialects and the Transformation of an Indo-European Process (1970), Comparative Studies in Greek and Indic Meter (1974), Pindar’s Homer: The Lyric Possession of an Epic Past (1990), Greek Mythology and Poetics (1990), Poetry as Performance: Homer and Beyond (1996), Homeric Questions (1996) Plato's Rhapsody and Homer's Music (2002), Homeric Responses (2004), and Homer's Text and Language (2004); he has as well edited or co-edited various volumes and written almost a hundred articles and reviews (see his CV for a full list). Professor Nagy has lectured widely in North America and Europe on a great range of keywords, especially concentrated in Homeric and Archaic Greek questions. Some of his recent lecture texts are available on the CHS website here. He is a strong proponent of the use of technology in teaching, and in the teaching and use of student writing in the core curriculum.</p>",
			"isAuthor": null,
			"tagline": "Born in Budapest, Hungary, Professor Nagy was educated at Indiana University and Harvard University, where he studied Classical Philology and Linguistics, receiving his Ph.D. in 1966.",
			"featureOnHomepage": true,
			"avatar": {
				"src": "https://archimedes-data003.s3-us-west-2.amazonaws.com/FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg",
				"filename": "IMG_1040 sel.jpg",
				"type": "image/jpeg",
				"size": 16360,
				"directive": "uploads",
				"key": "FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg"
			},
			"nCommentsTotal": 3212
		},
		{
			"_id": "WbivfcPiFmPMyvHoe",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Laura Slatkin",
			"slug": "laura-slatkin",
			"bio": "{\"html\":\"<p><strong>Laura M. Slatkin</strong> is a Professor of Classical Studies at New York University’s Gallatin School and Visiting Professor in the Committee on Social Thought, University of Chicago. She has taught at the University of California at Santa Cruz, Yale University, and Columbia University.  Her research and teaching interests include early Greek epic; wisdom traditions in classical and Near Eastern antiquity; and the reception of Homer. She has published articles on Greek epic and drama and most recently on &quot;British Romantic Homer&quot;;  <em>The Power of Thetis and Selected Essays</em> was published by the CHS in 2011.  She has served as the editor in chief of <em>Classical Philology</em>, and coedited <em>Histories of Post-War French Thought, Volume 2: Antiquities</em> (with G. Nagy and N. Loraux, New Press, 2001).  She is currently collaborating on a study of the reception of Homer in British romantic poetry.</p>\",\"raw\":{\"entityMap\":{},\"blocks\":[{\"key\":\"215ke\",\"text\":\"Laura M. Slatkin is a Professor of Classical Studies at New York University’s Gallatin School and Visiting Professor in the Committee on Social Thought, University of Chicago. She has taught at the University of California at Santa Cruz, Yale University, and Columbia University.  Her research and teaching interests include early Greek epic; wisdom traditions in classical and Near Eastern antiquity; and the reception of Homer. She has published articles on Greek epic and drama and most recently on \\\"British Romantic Homer\\\";  The Power of Thetis and Selected Essays was published by the CHS in 2011.  She has served as the editor in chief of Classical Philology, and coedited Histories of Post-War French Thought, Volume 2: Antiquities (with G. Nagy and N. Loraux, New Press, 2001).  She is currently collaborating on a study of the reception of Homer in British romantic poetry.\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":0,\"length\":16,\"style\":\"BOLD\"},{\"offset\":529,\"length\":39,\"style\":\"ITALIC\"},{\"offset\":645,\"length\":19,\"style\":\"ITALIC\"},{\"offset\":679,\"length\":59,\"style\":\"ITALIC\"}],\"entityRanges\":[],\"data\":{}}]}}",
			"isAuthor": null,
			"tagline": "Laura M. Slatkin is a Professor of Classical Studies at New York University’s Gallatin School and Visiting Professor in the Committee on Social Thought, University of Chicago.",
			"featureOnHomepage": null,
			"nCommentsTotal": null
		},
		{
			"_id": "9FGsiaCaNTx8mfGEX",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Leonard Muellner",
			"slug": "leonard-muellner",
			"bio": "<p>Leonard Muellner is Professor of Classical Studies Emeritus at Brandeis University and Director for IT and Publications at the Center for Hellenic Studies. Educated at Harvard (Ph.D. 1973), his scholarly interests center on Homeric epic, with special interests in historical linguistics, anthropological approaches to the study of myth, and the poetics of oral traditional poetry. His recent work includes \\\"Grieving Achilles,\\\" in Homeric Contexts: Neoanalysis and the Interpretation of Oral Poetry, ed. A. Rengakos, F. Montanari, and C. Tsagalis, Trends in Classics, Supplementary Volume 12, Berlin, 2012, pp. 187-210, and “Homeric Anger Revisited,” Classics@ Issue 9: Defense Mechanisms, Center for Hellenic Studies, Washington, DC, September, 2011.</p>",
			"isAuthor": null,
			"tagline": "Leonard Muellner is Professor of Classical Studies Emeritus at Brandeis University and Director for IT and Publications at the Center for Hellenic Studies.",
			"avatar": {
				"src": "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ/leonard_muellner.jpg",
				"filename": "leonard_muellner.jpg",
				"type": "image/jpeg",
				"size": 23777,
				"directive": "uploads",
				"key": "JnSr8x5kfWvmeraLZ/leonard_muellner.jpg"
			},
			"featureOnHomepage": true,
			"nCommentsTotal": 1184
		},
		{
			"_id": "rfjtc3BHeenvSm48c",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Mary Ebbott",
			"slug": "mary-ebbott",
			"bio": "<p>Mary Ebbott is Associate Professor of Classics at the College of the Holy Cross in Worcester, Massachusetts. She is the author of Imagining Illegitimacy in Greek Literature (Lexington Books, 2003) and co-author, with Casey Dué, of Iliad 10 and the Poetics of Ambush (Center for Hellenic Studies, 2010).</p>",
			"isAuthor": null,
			"tagline": "Mary Ebbott is Associate Professor of Classics at the College of the Holy Cross in Worcester, Massachusetts",
			"featureOnHomepage": false,
			"nCommentsTotal": 1
		},
		{
			"_id": "DqsynWCzwotppKeTE",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Olga Levaniouk",
			"slug": "olga-levaniouk",
			"bio": "<p>At the center of my research is Homer as part of the song culture of Ancient Greece. My interests include myth, ritual, lyric poetry, drama, comparative and historical linguistics, oral traditional poetry and poetics in Greece and beyond, and a comparative approach to all of the above. I am particularly interested in comparative work involving Indic and Slavic poetry, and have also made forays into working with Turkic epic.  I have at various points studied Sanskrit, Avestan, Hittite and Tocharian and retain various degrees of competency (and unflagging interest!) in these languages.  On the myth side of things, I have a long-standing interest in mythological variation and local mythologies. On the poetic side, I have a particular interests in women's songs, especially laments and wedding songs.</p><p>My first book, Eve of the Festival: Making Myth in Odyssey 19, is a study of myth in Homer on the example of the first dialogue between Penelope and Odysseus.</p><p>My current project is a comparative study of Greek weddings focusing on the performances by the bride, her mother, and her age-mates.</p>",
			"isAuthor": null,
			"tagline": "At the center of my research is Homer as part of the song culture of Ancient Greece. My interests include myth, ritual, lyric poetry, drama, comparative and historical linguistics, oral traditional poetry and poetics in Greece and beyond, and a comparative approach to all of the above.",
			"featureOnHomepage": null,
			"nCommentsTotal": 0
		},
		{
			"_id": "XxAj5sMLgKoWdtvdx",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Richard Martin",
			"slug": "richard-martin",
			"bio": "<p>Richard P. Martin is the Antony and Isabelle Raubitschek Professor in Classics at Stanford University. He works mainly on Greek poetry (especially Homer and Aristophanes), religion, and mythology.</p>",
			"isAuthor": null,
			"tagline": "Richard P. Martin is the Antony and Isabelle Raubitschek Professor in Classics at Stanford University",
			"featureOnHomepage": false,
			"avatar": {
				"src": "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ / richard_martin.jpg",
				"filename": "richard_martin.jpg",
				"type": "image/jpeg",
				"size": 11617,
				"directive": "uploads",
				"key": "JnSr8x5kfWvmeraLZ / richard_martin.jpg"
			},
			"nCommentsTotal": 0
		},
		{
			"_id": "Rb6uTSgH8hv8qce2A",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Thomas Walsh",
			"slug": "thomas-walsh",
			"bio": null,
			"isAuthor": null,
			"tagline": null,
			"featureOnHomepage": null,
			"nCommentsTotal": null
		},
		{
			"_id": "wM3KK4sAvaY2Sn2Wc",
			"tenantId": "4tJPty4gis3AhGt6z",
			"name": "Yiannis Petropoulos",
			"slug": "yiannis-petropoulos",
			"bio": null,
			"isAuthor": null,
			"tagline": null,
			"featureOnHomepage": null,
			"nCommentsTotal": null
		}
	],
	pindar: [
		{
			"_id": "447bAEWTRMWh5MvjH",
			"tenantId": "km6CCNmQF7WEg7nce",
			"name": "Maša Culumovic",
			"slug": "ma-a-culumovic",
			"bio": "<p><br></p>",
			"isAuthor": null,
			"tagline": null,
			"featureOnHomepage": null,
			"nCommentsTotal": null
		},
		{
			"_id": "ybNNxCRGaQcuAEj6p",
			"tenantId": "km6CCNmQF7WEg7nce",
			"name": "Gregory Nagy",
			"slug": "gregory-nagy-1",
			"bio": null,
			"isAuthor": null,
			"tagline": null,
			"featureOnHomepage": true,
			"avatar": {
				"src": "https://archimedes-data003.s3-us-west-2.amazonaws.com/FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg",
				"filename": "IMG_1040 sel.jpg",
				"type": "image/jpeg",
				"size": 16360,
				"directive": "uploads",
				"key": "FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg"
			},
			"nCommentsTotal": null
		},
	],
	pausanias: [
		{
			"_id": "C6iThiXKvTiPfwDxs",
			"tenantId": "5Ns98kBwQAvi6sEyK",
			"name": "Carolyn Higbie",
			"slug": "carolyn-higbie",
			"bio": "{\"html\":\"<p></p>\",\"raw\":{\"entityMap\":{},\"blocks\":[{\"key\":\"1dvos\",\"text\":\"\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}}",
			"isAuthor": null,
			"tagline": null,
			"featureOnHomepage": null,
			"nCommentsTotal": null
		},
		{
			"_id": "Qz7uuTjhWh4rkagnL",
			"tenantId": "5Ns98kBwQAvi6sEyK",
			"name": "Gregory Nagy",
			"slug": "gregory-nagy-2",
			"bio": "{\"html\":\"<p></p>\",\"raw\":{\"entityMap\":{},\"blocks\":[{\"key\":\"i5qt\",\"text\":\"\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}}",
			"isAuthor": null,
			"tagline": null,
			"featureOnHomepage": null,
			"avatar": {
				"src": "https://archimedes-data003.s3-us-west-2.amazonaws.com/FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg",
				"filename": "IMG_1040 sel.jpg",
				"type": "image/jpeg",
				"size": 16360,
				"directive": "uploads",
				"key": "FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg"
			},
			"nCommentsTotal": null
		},
		{
			"_id": "FxRrBGuLrhJHxWCPq",
			"tenantId": "5Ns98kBwQAvi6sEyK",
			"name": "Greta Hawes",
			"slug": "greta-hawes",
			"bio": "{\"html\":\"<p></p>\",\"raw\":{\"entityMap\":{},\"blocks\":[{\"key\":\"am0j\",\"text\":\"\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}}",
			"isAuthor": null,
			"tagline": null,
			"featureOnHomepage": true,
			"nCommentsTotal": null
		},
		{
			"_id": "CJxTvsr8Kze5Y4pe4",
			"tenantId": "5Ns98kBwQAvi6sEyK",
			"name": "Lia Hanhardt",
			"slug": "lia-hanhardt",
			"bio": "{\"html\":\"<p></p>\",\"raw\":{\"entityMap\":{},\"blocks\":[{\"key\":\"bg5is\",\"text\":\"\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}}",
			"isAuthor": null,
			"tagline": "",
			"featureOnHomepage": false,
			"nCommentsTotal": null
		},
		{
			"_id": "hQmN8ArpAeg9xgQhC",
			"tenantId": "5Ns98kBwQAvi6sEyK",
			"name": "Scott Smith",
			"slug": "scott-smith",
			"bio": "{\"html\":\"<p></p>\",\"raw\":{\"entityMap\":{},\"blocks\":[{\"key\":\"ddugk\",\"text\":\"\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}]}}",
			"isAuthor": null,
			"tagline": null,
			"featureOnHomepage": true,
			"nCommentsTotal": null
		},
	],
};

export default commenters;
