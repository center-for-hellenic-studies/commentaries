import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'react-apollo';

// component
import CommenterList from '../../components/CommenterList';

// lib
import commenters from '../../lib/commenters';
import getCurrentProjectHostname from '../../../../lib/getCurrentProjectHostname';



const CommenterListContainer = props => {
	let subdomain = getCurrentProjectHostname().split('.')[0];
	let _commenters = commenters[subdomain] || [];

	if (props.featureOnHomepage) {
		 _commenters = _commenters.filter(commenter => (
			commenter.featureOnHomepage === true
		));
	}

	return (
		<CommenterList
			{...props}
			commenters={_commenters}
		/>
	);
};


const mapStateToProps = (state, props) => ({
	tenantId: state.tenant.tenantId,
});

export default compose(
	connect(mapStateToProps),
)(CommenterListContainer);
