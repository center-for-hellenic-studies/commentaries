import { gql, graphql } from 'react-apollo';

// lib
import getCurrentProjectHostname from '../../../../lib/getCurrentProjectHostname';


const query = gql`
query commenters ($hostname: String) {
  project(hostname: $hostname) {
		id
	  commenters {
	    id
	    username
	    firstName
	    lastName
	    picture
	  }
	}
}`;

const commentersQuery = graphql(query, {
	name: 'commentersQuery',
	options: () => ({
		variables: {
			hostname: getCurrentProjectHostname().split('.')[0],
		}
	}),
});



export default commentersQuery;
