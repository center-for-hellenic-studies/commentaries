// TODO rename to TextsListContainer

import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import InfiniteScroll from 'react-infinite-scroller';
import qs from 'qs-lite';
import { compose } from 'react-apollo';
import ReactTimeout from 'react-timeout'
import Button from '@material-ui/core/Button';

import CardLoadingItem from '../../../../components/common/cards/CardLoadingItem';
import List from '../../../../components/common/lists/List';
import itemListQuery from '../../graphql/queries/list';


// import Collections from '../../graphql/texts'

class TextsListContainer extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);

		this.state = {
			isThrottled: false,
			offset: 1,
			limit: 5000,
		};

	}

	clearFilters() {
		window.location.replace(window.location.pathname);
	}

	async handleLoadMore(hasMoreToLoad) {
		const { isThrottled } = this.state;

		if (!hasMoreToLoad) {
			return false;
		}

		if (!isThrottled) {
			await this.setState({
				isThrottled: true,
			});

			const query = qs.parse(window.location.search.replace('?', ''));

			if (query.page) {
				query.page = parseInt(query.page, 10) + 1;
			} else {
				query.page = 2;
			}

			this.setState({offset: query.page})

			window.location.replace(window.location.pathname + '?' + qs.stringify(query));
		}
	}

	render() {
		let items = this.props.items || [];
		let itemsCount = items.length;

		let displayedItems = items.slice(0, this.state.offset*this.state.limit);
		let hasMoreToLoad = (this.state.offset*this.state.limit) < itemsCount;


		if (!items || !items.length) {
			return (
				<div className="itemListContainer -no-results">
					<p>
						No items found with matching filters.
					</p>
					<Button
						onClick={this.clearFilters}
						size="large"
						variant="outlined"
					>
						Clear filters
					</Button>
				</div>
			);
		}

		return (
			<div className="itemListContainer">
				<InfiniteScroll
					pageStart={0}
					loadMore={this.handleLoadMore.bind(this, hasMoreToLoad)}
					hasMore={hasMoreToLoad}
					loader={<div className="listItem" key="x"><CardLoadingItem /></div>}
				>
					<List
						items={displayedItems}
						handleSelect={this.handleSelect}

					/>
				</InfiniteScroll>
			</div>
		);
	}
}

TextsListContainer.propTypes = {
	itemListQuery: PropTypes.object,
	selectable: PropTypes.bool,
	handleSelect: PropTypes.func,
	forceLoading: PropTypes.bool,
	isAdmin: PropTypes.bool,

};

export default TextsListContainer;
