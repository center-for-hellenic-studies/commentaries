/**
 * @prettier
 */

import React, { useEffect, useState } from 'react';
import {
	BrowserRouter as Router,
	Route,
	Switch,
	useLocation,
} from 'react-router-dom';

import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { makeStyles } from '@material-ui/core/styles';
import { useLazyQuery, useQuery } from '@apollo/react-hooks';

import Root from '../../../../containers/Root';
import FacetedCards from '../../../../components/common/FacetedCards';
import Filters from '../../../../components/common/Filters';
import textGroupsQuery from '../../../reading/graphql/queries/textGroups';

const collectionsQuery = gql`
	query collections(
		$textSearch: String
		$urn: TEXT_CtsUrn
		$limit: Int
		$offset: Int
	) {
		TEXT_collections(
			textsearch: $textSearch
			urn: $urn
			limit: $limit
			offset: $offset
		) {
			id
			repository
			slug
			title
			urn
			textGroups {
				collectionId
				id
				slug
				title
				urn
			}
		}
	}
`;

const worksByTextGroupsQuery = gql`
	query worksByTextGroupsQuery(
		$textSearch: String
		$language: String
		$urn: TEXT_CtsUrn
		$urns: [TEXT_CtsUrn]
		$limit: Int
		$offset: Int
	) {
		TEXT_textGroups(
			textsearch: $textSearch
			urn: $urn
			urns: $urns
			limit: $limit
			offset: $offset
		) {
			collectionId
			id
			slug
			title
			urn
			works(language: $language) {
				description
				english_title
				form
				full_urn
				id
				label
				original_title
				slug
				urn
				work_type
				language {
					id
					title
					slug
				}
				version {
					description
					id
					slug
					title
					urn
				}
				exemplar {
					description
					id
					slug
					title
					urn
				}
				translation {
					description
					id
					slug
					title
					urn
				}
			}
		}
	}
`;

function useSearch() {
	return new URLSearchParams(useLocation().search);
}

const useStyles = makeStyles({
	card: {
		marginBottom: '2em',
		minWidth: 275,
	},
	bullet: {
		display: 'inline-block',
		margin: '0 2px',
		transform: 'scale(0.8)',
	},
	title: {
		fontSize: 14,
	},
	pos: {
		marginBottom: 12,
	},
});

const WorksList = ({ className, loading, works }) => {
	return (
		<section style={{ flexGrow: 2 }}>
			<FacetedCards
				classes={className}
				isAdmin={true}
				items={works}
				loading={loading}
				selectable={false}
			/>
		</section>
	);
};

const TextGroupContainer = () => {
	const query = useSearch();
	const collectionTitles = query.getAll('Collection');
	const textGroupLanguages = query.getAll('Language');
	const textGroupSources = query.getAll('Source');
	const classes = useStyles();
	const [works, setWorks] = useState([]);
	const {
		data: collectionsData,
		error: collectionsError,
		loading: collectionsLoading,
	} = useQuery(collectionsQuery);
	const {
		data: groupsData,
		error: groupsError,
		loading: groupsLoading,
	} = useQuery(textGroupsQuery);
	const [getWorksBySelectedGroups, { worksLoading }] = useLazyQuery(
		worksByTextGroupsQuery,
		{
			onCompleted: data => {
				setWorks(data.TEXT_textGroups.flatMap(tg => tg.works));
			},
		}
	);
	const { TEXT_collections: collections = [] } = collectionsData || {};
	let { TEXT_textGroups: textGroups = [] } = groupsData || {};

	useEffect(() => {
		if (textGroupSources.length || collectionTitles.length) {
			const selectedTextGroups = textGroups.filter(t =>
				textGroupSources.includes(t.title)
			);
			let selectedTextGroupUrns = selectedTextGroups.map(t => t.urn);
			const selectedLanguage = textGroupLanguages[0];

			if (collectionTitles) {
				selectedTextGroupUrns = Array.from(
					new Set([
						...selectedTextGroupUrns,
						...collections
							.filter(c => collectionTitles.includes(c.title))
							.flatMap(c => c.textGroups.map(t => t.urn)),
					])
				);
			}

			getWorksBySelectedGroups({
				variables: { language: selectedLanguage, urns: selectedTextGroupUrns },
			});
		} else {
			setWorks([]);
		}
	}, [
		collectionTitles.join(''),
		textGroupSources.join(''),
		textGroupLanguages.join(''),
	]);

	if (groupsLoading) {
		return (
			<div>
				<span>Loading text groups...</span>
			</div>
		);
	}

	if (groupsError) {
		return (
			<div>
				<span className="error">Something bad happened: {error.message}</span>
			</div>
		);
	}

	if (textGroups.length === 0) {
		return (
			<div>
				<span className="error">We didn't find any textGroups.</span>
			</div>
		);
	}

	return (
		<div className="flex">
			<WorksList
				className={classes.card}
				loading={worksLoading}
				works={works
					.filter(w => Boolean(w.description && w.full_urn))
					.map(w => ({ ...w, url: `/read/${w.full_urn}` }))}
			/>
			<section>
				<Filters
					filters={[
						{ name: 'Collection', values: collections.map(c => c.title) },
						{ name: 'Source', values: textGroups.map(tg => tg.title) },
						{
							name: 'Language',
							values: textGroups.flatMap(tg =>
								Array.from(new Set(tg.works.map(w => w.language.slug)))
							),
						},
					]}
					classes={[]}
					loading={groupsLoading}
					relevantFilters={[]}
				/>
			</section>
		</div>
	);
};

TextGroupContainer.propTypes = {
	match: PropTypes.shape({
		params: PropTypes.shape({
			id: PropTypes.string,
		}),
	}),
};

const TextFiltersRoutes = props => (
	<Root>
		<Router>
			<Switch>
				<Route path="/texts" component={TextGroupContainer} />
			</Switch>
		</Router>
	</Root>
);

export default TextFiltersRoutes;
