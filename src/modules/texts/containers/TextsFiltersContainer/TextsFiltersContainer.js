import React from 'react';
import { compose } from 'react-apollo';

import Filters from '../../../../components/common/Filters';


class TextFiltersContainer extends React.Component {
	constructor(props) {
		super(props);
	}

	render () {
		let filters = this.props.filters || [];
		let relevantFilters = [];

		let filterLookup = {
			language: {
				name: 'Language',
				type: 'text',
				values: []
			},
			work_type: {
				name: 'Work Type',
				type: 'text',
				values: []
			},
			structure: {
				name: 'Structure',
				type: 'text',
				values: []
			},
			collection: {
				name: 'Collection',
				type: 'text',
				values: []
			},
			textGroup: {
				name: 'Author',
				type: 'text',
				values: []
			}

		}

		if (this.props.items) {
			this.props.items.forEach((text) => {
				if (text.language && text.language.title && !filterLookup.language.values.includes(text.language.title)) {
					filterLookup.language.values.push(text.language.title);
				}
				if (text.work_type && text.work_type.length > 0 && !filterLookup.work_type.values.includes(text.work_type)) {
					filterLookup.work_type.values.push(text.work_type);
				}
				if (text.structure && text.structure.length > 0 && !filterLookup.structure.values.includes(text.structure)) {
					filterLookup.structure.values.push(text.structure);
				}
				if (text.collection && text.collection.length > 0 && !filterLookup.collection.values.includes(text.collection)) {
					filterLookup.collection.values.push(text.collection);
				}
				if (text.textGroup && text.textGroup.length > 0 && !filterLookup.textGroup.values.includes(text.textGroup)) {
					filterLookup.textGroup.values.push(text.textGroup);
				}
			})
		}


		Object.keys(filterLookup).forEach((filter_name) => {
			relevantFilters.push(filterLookup[filter_name])
		})


		return (
			<div>
				{this.props.loading ? (
					<Filters
						filters={filters}
						loading
					/>
				) : (
					<Filters
						filters={filters}
						relevantFilters={relevantFilters}
					/>
				)}
			</div>
		);
	}
}


export default TextFiltersContainer;
