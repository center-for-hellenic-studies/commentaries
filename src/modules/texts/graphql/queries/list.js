import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import qs from 'qs-lite';

import getCurrentArchiveHostname from '../../../../lib/getCurrentArchiveHostname';


const query = gql`
	query itemListQuery ($offset: Int, $limit: Int) {
		  TEXT_worksCount
		  TEXT_works(offset: $offset, limit: $limit) {
			id
			original_title
			form
			
			english_title
			structure
			slug
			description
				filename
			work_type
			label
			description
			
			language {
			id
			title
			slug
			}
			translation {
			id
			title
			slug
			description
			urn
			}
		}
	}
`;

const itemListQuery = graphql(query, {
	name: 'itemListQuery',
	options: () => {
	  const query = qs.parse(window.location.search.replace('?', ''));
		let offset = 0;
	  	let limit = 10;
		let textsearch;
		let filter;
		let tags;

	  if (query.page) {
	    limit = query.page * limit;
	  }

		if (query.search) {
			textsearch = query.search;
		}

		if (query.Tags) {
			tags = query.Tags;
		}

		if (query.dateFields) {
			query.dateFields.split('+').forEach((fieldTitle) => {
				delete query[fieldTitle+'_Max'];
				delete query[fieldTitle+'_Min'];
			})
			delete query['dateFields'];
		}

		for (let key in query) {
			if (['page', 'search', 'Tags'].indexOf(key) < 0) {
				if (!filter) {
					filter = [];
				}

				filter.push({
					name: key,
					values: query[key].replace('%20', ' ').split('+'),
				});
			}
		}

		// exception for storybook
		if (window.location.host === 'localhost:9009') {
			filter = null;
		}

		return {
			variables: {
				hostname: getCurrentArchiveHostname(),
				textsearch,
				tags,
				filter,
				offset,
				limit,
			},
		};
	},
});

export default itemListQuery;
