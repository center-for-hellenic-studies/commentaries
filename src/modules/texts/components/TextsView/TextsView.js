import React from 'react';

import Root from '../../../../containers/Root';
import TextsFacetedCards from '../../components/TextsFacetedCards';


const TextsView = () => (
	<Root>
		<TextsFacetedCards />
	</Root>
);

export default TextsView;
