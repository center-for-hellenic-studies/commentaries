import React from 'react';
import { Route } from 'react-router';

const AlexEditing = () => {
	return window.location = "http://homer.beta.newalexandria.info/read/urn:cts:greekLit:tlg0012.tlg001.perseus-grc2:1.1-1.30";
};


export default (
	<Route
		exact
		path="/textNodes/edit"
		component={AlexEditing}
		roles={['commenter', 'editor', 'admin']}
	/>
);
