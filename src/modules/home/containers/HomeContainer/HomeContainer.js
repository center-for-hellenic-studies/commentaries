import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'react-apollo';

// graphql
import projectQueryByHostname from '../../../settings/graphql/queries/detail';

// component
import Home from '../../components/Home';


const HomeContainer = ({ projectQueryByHostname }) => {
	let project = null;

	if (
		projectQueryByHostname
    && projectQueryByHostname.project
	) {
		project = projectQueryByHostname.loading ? null : projectQueryByHostname.project;
	}

	if (!project) {
		return null;
	}

	return (
		<Home
			project={project}
		/>
	);
}

export default compose(
	projectQueryByHostname,
)(HomeContainer);
