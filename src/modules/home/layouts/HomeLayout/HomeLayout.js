import React from 'react';
import $ from 'jquery';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'react-apollo';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// lib
import muiTheme from '../../../../lib/muiTheme';

// graphql
import projectsQuery from '../../../settings/graphql/queries/detail';

// layouts
import Header from '../../../../components/navigation/Header';
import Footer from '../../../../components/navigation/Footer';
/*
import CommunityLayout from '../../../community/layouts/CommunityLayout/CommunityLayout';
import NameResolutionServiceLayout from '../../../nrs/layouts/NameResolutionServiceLayout';
*/

// components
import HomeContainer from '../../containers/HomeContainer';
import LoadingHome from '../../../../components/loading/LoadingHome';

// auth
// import AuthModalContainer from '../../../../modules/auth/containers/AuthModalContainer';


class HomeLayout extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			filters: [],
			settings: [],
		};
	}

	componentDidMount() {
		if (typeof window.location.hash !== 'undefined' && window.location.hash.length > 0) {
			setTimeout(() => {
				if ($(window.location.hash).offset()) {
					$('html, body').animate({ scrollTop: $(window.location.hash).offset().top - 100 }, 300);
				}
			}, 1000);
		}
	}

	render() {
		let project = null;
		const { projectQueryByHostname } = this.props;

		if (
			projectQueryByHostname
			&& projectQueryByHostname.project
		) {
			project = projectQueryByHostname.project;
		}

		if (!project) {
			return <LoadingHome />;
		}


		/**
		if (
			project.subdomain === 'nrs'
			|| project.subdomain === 'nrs2'
		) {
			return <NameResolutionServiceLayout />;
		}

		if (project.isAnnotation) {
			return <CommunityLayout />;
		}
		*/

		return (
			<MuiThemeProvider muiTheme={getMuiTheme(muiTheme)}>
				<div className="chs-layout home-layout">
					<Header
						isOnHomeView
						history={this.props.history}
					/>

					<HomeContainer
						history={this.props.history}
					/>

					<Footer />
					{/* <AuthModalContainer /> */}
				</div>
			</MuiThemeProvider>
		);
	}

}

HomeLayout.propTypes = {
	signup: PropTypes.func,
	showForgotPwd: PropTypes.func,
	history: PropTypes.any
};

export default compose(
	projectsQuery,
)(HomeLayout);
